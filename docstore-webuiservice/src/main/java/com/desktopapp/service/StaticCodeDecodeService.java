package com.desktopapp.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.docstore.domainobjs.json.CodeDataItemDO;
import org.docstore.domainobjs.json.StaticCodeDecodeDO;


/**
 * Static Code Decode Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class StaticCodeDecodeService extends AbstractService<StaticCodeDecodeDO> {
	
	public StaticCodeDecodeService() {
        super(StaticCodeDecodeDO.class);
    }
    
	public static final String BASE_RESOURCE = "staticCodeDecodes";
	
    @Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

    public List<CodeDataItemDO> getStaticCodeDataItemList(String codeName,String language){
	   
    	List<CodeDataItemDO> codeDataItemList = null;
    	String getCallWebURL = getWebserviceURL() + "/{codeName}/{language}";
    	
    	Map<String, String> params = new HashMap<String, String>();
        params.put("codeName", codeName);
        params.put("language", language);
    	
    	//CodeDataItemDO[] codeDecodes = getRestTemplate().getForObject(getCallWebURL,CodeDataItemDO[].class,params);
		CodeDataItemDO[] codeDecodes = (CodeDataItemDO[])callService(getCallWebURL,CodeDataItemDO[].class,params);
    	
    	if(codeDecodes != null && codeDecodes.length != 0){
    		codeDataItemList = Arrays.asList(codeDecodes);
    	}else{
    		return codeDataItemList;
    	}
    	
    	return codeDataItemList;
    	
    }


}
