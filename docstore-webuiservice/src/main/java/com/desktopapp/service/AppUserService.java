/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.service;

import org.docstore.domainobjs.json.AppUserDO;

/**
 * AppUser Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserService extends AbstractService<AppUserDO> {

    public AppUserService() {
        super(AppUserDO.class);
    }
    
   public static final String BASE_RESOURCE = "appUser";
	
    @Override
	protected String getBaseResource() {
		return BASE_RESOURCE;
	}

}
