package com.desktopapp.fx.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.desktopapp.fx.controller","com.desktopapp.service","com.docstore.jpa.spring.extended"})
public class AppConfigTest {

}