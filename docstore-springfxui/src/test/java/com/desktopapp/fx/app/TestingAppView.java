package com.desktopapp.fx.app;

import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.mvc.View;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;

/**
 * Created by diwakar009 on 12/12/2018.
 */
public class TestingAppView extends BorderPane {

    private BorderPane appCenter;

    public TestingAppView() {
        super();
        buildView();
    }

    private void buildView() {
        getStyleClass().add("app-view");
        setPadding(new Insets(10, 0, 0, 0));
        setCenter(buildAppCenter());
    }

    private BorderPane buildAppCenter() {
        appCenter = new BorderPane();
        appCenter.setId("app-center");
        return appCenter;
    }

    public void addPageToCenter(View page) {
        appCenter.setLeft(null);
        appCenter.setRight(null);
        appCenter.setTop(null);
        appCenter.setCenter(page.asNode());
        appCenter.setBottom(null);

        if (page instanceof DataPageView) {
            ((DataPageView) page).setDataPageAccelerators();
        }
        page.asNode().requestFocus();
    }
}
