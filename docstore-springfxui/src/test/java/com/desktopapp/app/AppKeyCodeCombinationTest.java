package com.desktopapp.app;

import com.desktopapp.fx.app.AppKeyCode;
import com.desktopapp.fx.config.AppConfigTest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by diwakar009 on 22/6/2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AppConfigTest.class })
public class AppKeyCodeCombinationTest {

    @Test
    public void keyCodeTest() throws Exception {
        Assert.assertEquals("Alt+Q", AppKeyCode.getKeyCode("ALT-Q").getDisplayText());
        Assert.assertEquals("Alt+Z", AppKeyCode.getKeyCode("ALT-Z").getDisplayText());
        Assert.assertEquals("F2", AppKeyCode.getKeyCode("F2").getDisplayText());
        Assert.assertEquals("Ctrl+Z", AppKeyCode.getKeyCode("CTRL-Z").getDisplayText());
    }
}
