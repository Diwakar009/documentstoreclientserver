/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.control;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

/**
 * Tabbed pane
 *
 * @author R Diwakar C
 */
public class TabbedPane extends TabPane {

    private static final String DEFAULT_STYLE_CLASS = "tabbed-pane";

    public TabbedPane() {
        this((Tab[])null);
    }

    public TabbedPane(Tab... tabs) {
        super(tabs);
        getStyleClass().add(DEFAULT_STYLE_CLASS);
    }
}
