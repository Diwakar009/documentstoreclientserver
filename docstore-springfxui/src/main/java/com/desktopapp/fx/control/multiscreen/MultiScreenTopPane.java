package com.desktopapp.fx.control.multiscreen;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * Created by diwakar009 on 12/7/2019.
 */
public class MultiScreenTopPane extends StackPane {
    private String title;
    private Label lblTitle;

    public MultiScreenTopPane(String title) {
        super();
        this.title = title;
        lblTitle = new Label(title);
        init();
    }

    private void init(){
       getChildren().add(lblTitle);
    }
}
