package com.desktopapp.fx.control;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.mvc.AbstractDataPageView;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import org.docstore.domainobjs.json.DOPage;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by diwakar009 on 24/9/2019.
 */
public class TableDataView<T> extends BorderPane{

    private final Logger logger = LoggerFactory.getLogger(TableDataView.class);

    private SearchField searchField;
    private ProgressIndicator progressIndicator;
    private TableView tableView;
    private Pager pager;

    private Button btnAddNew;
    private Button btnEdit;
    private Button btnDelete;
    private Button btnPrintPreview;
    private Button btnPrint;
    private Button btnPdf;

    private ITableDataViewBehaviour<T> behaviour;

    public TableDataView(ITableDataViewBehaviour behaviour) {
        super();
        this.behaviour = behaviour;
        init();
    }

    public void init(){
        buildView();
    }

    public void buildView(){
        getStyleClass().addAll("data-page-view");
        setTop(getHeaderBar());
        setCenter(getTablePane());
    }

    private HBox getHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("data-page-header");

        Label title = ViewHelpers.createFontIconLabel(behaviour.getTitle(), behaviour.getFontIcon(), "");
        title.getStyleClass().add("page-title");

        progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(50, 50);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        // actions
        btnAddNew = new Button("", FontIconFactory.createIcon(AppFontIcons.ADD_NEW));
        btnAddNew.setTooltip(new Tooltip(I18n.COMMON.getString("action.addNew")));
        btnAddNew.getStyleClass().add("left-pill");
        btnAddNew.setOnAction(event -> behaviour.onAddNew());

        btnEdit = new Button("", FontIconFactory.createIcon(AppFontIcons.EDIT));
        btnEdit.setTooltip(new Tooltip(I18n.COMMON.getString("action.edit")));
        btnEdit.getStyleClass().add("right-pill");
        btnEdit.setOnAction(event -> behaviour.onEdit());
        HBox.setMargin(btnEdit, new Insets(0, 10, 0, 0));

        btnDelete = new Button("", FontIconFactory.createIcon(AppFontIcons.DELETE));
        btnDelete.setTooltip(new Tooltip(I18n.COMMON.getString("action.delete")));
        btnDelete.setOnAction(event -> behaviour.onDelete());
        HBox.setMargin(btnDelete, new Insets(0, 10, 0, 0));

        headerBar.getChildren().addAll(title, progressIndicator, spacer);

        if(behaviour.hasAddNewActions()) {
            headerBar.getChildren().add(btnAddNew);
        }

        if(behaviour.hasEditActions()) {
            headerBar.getChildren().add(btnEdit);
        }

        if(behaviour.hasDeleteActions()) {
            headerBar.getChildren().add(btnDelete);
        }

        //headerBar.getChildren().addAll(title, progressIndicator, spacer, btnAddNew,btnAddNew, btnDelete);
        if (behaviour.hasPrintActions()) {
            btnPrintPreview = new Button("", FontIconFactory.createIcon(AppFontIcons.PRINT_PREVIEW));
            btnPrintPreview.setTooltip(new Tooltip(I18n.COMMON.getString("action.printPreview")));
            btnPrintPreview.getStyleClass().add("left-pill");
            btnPrintPreview.setOnAction(event -> behaviour.onPrintPreview());

            btnPrint = new Button("", FontIconFactory.createIcon(AppFontIcons.PRINT));
            btnPrint.setTooltip(new Tooltip(I18n.COMMON.getString("action.print")));
            btnPrint.getStyleClass().add("center-pill");
            btnPrint.setOnAction(event -> behaviour.onPrint());

            btnPdf = new Button("", FontIconFactory.createIcon(AppFontIcons.PDF));
            btnPdf.setTooltip(new Tooltip(I18n.COMMON.getString("action.pdf")));
            btnPdf.getStyleClass().add("right-pill");
            btnPdf.setOnAction(event -> behaviour.onPdf());
            HBox.setMargin(btnPdf, new Insets(0, 10, 0, 0));

            headerBar.getChildren().addAll(btnPrintPreview, btnPrint, btnPdf);
        }

        // search field
        searchField = new SearchField(300);
        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            pager.setCurrentPageIndex(1);
            refreshData();
        });
        HBox.setMargin(searchField, new Insets(0, 0, 0, 100));
        headerBar.getChildren().add(searchField);
        return headerBar;
    }

    private VBox getTablePane() {
        logger.debug("get table pane");
        tableView = new TableView<>();
        tableView.setPlaceholder(new Label(I18n.COMMON.getString("tableView.dataNotFound")));
        tableView.setEditable(false);
        tableView.setTableMenuButtonVisible(true);

        List<TableColumn<T, ?>> columns = behaviour.getTableViewColumns();
        columns.forEach(col -> {
            if(col.getPrefWidth() == 0.0) {
                col.prefWidthProperty().bind(getTableView().widthProperty().divide(columns.size()));
            }
        });

        tableView.getColumns().addAll(columns);

        tableView.setOnMouseClicked((MouseEvent event) -> {
            if (event.getClickCount() > 1) {
                behaviour.onEdit();
            }
        });

        tableView.setOnKeyPressed((KeyEvent event) ->{
            // Get the Type of the Event
            String type = event.getEventType().getName();
            // Get the KeyCode of the Event
            KeyCode keyCode = event.getCode();
            if (event.getEventType() == KeyEvent.KEY_PRESSED && event.getCode() == KeyCode.ENTER)
            {
                behaviour.onEdit();
                event.consume();
            }
        });

        tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            behaviour.onSelectedItemChanged();
        });
        pager = new Pager(DesktopFxApplication.getLoginSession().getPreferences().getItemsPerPage(),this::refreshData);

        VBox tablePane = new VBox();
        tablePane.getStyleClass().add("data-page-center");
        tablePane.getChildren().addAll(tableView, pager);
        VBox.setVgrow(tableView, Priority.ALWAYS);
        VBox.setVgrow(pager, Priority.NEVER);
        VBox.setMargin(pager, new Insets(10, 10, 10, 10));
        return tablePane;
    }

    /**
     * Refreshs the data of the {@link TableView} and {@link Pager}
     */
    public void refreshData() {
        logger.debug("refresh data");
        Task<DOPage<T>> task = new Task<DOPage<T>>() {
            @Override
            protected DOPage<T> call() throws Exception {
                PageDetails details = new PageDetails();
                details.setStart(pager.getStartPosition());
                details.setEnd(pager.getEndPosition());
                details.setPageNumber(pager.getCurrentPageIndex() - 1);
                details.setSize(pager.getItemsPerPage());
                //return getController().getData(searchField.getText(),details);
                return (DOPage<T>)behaviour.getData(searchField.getText(),details);
            }
        };
        task.stateProperty().addListener((source, oldState, newState) -> {
            if (newState.equals(Worker.State.SUCCEEDED)) {
                tableView.getItems().clear();
                if (task.getValue() != null) {
                    DOPage<T> page = task.getValue();
                    List<T> tableRowMapDOList = page.getList();
                    tableRowMapDOList.forEach(tableRowMapDO -> {
                        tableView.getItems().addAll(behaviour.mapTableRowToDO((TableRowMapDO<String, String>)tableRowMapDO));
                    });
                    tableView.getSelectionModel().selectFirst();
                    pager.setRowCount(page.getTotalElements().intValue());
                }
            }
        });

        progressIndicator.visibleProperty().bind(task.runningProperty());
        new Thread(task).start();
    }

    public ITableDataViewBehaviour<T> getBehaviour() {
        return behaviour;
    }

    public void setBehaviour(ITableDataViewBehaviour<T> behaviour) {
        this.behaviour = behaviour;
    }

    public TableView getTableView() {
        return tableView;
    }

    public void setTableView(TableView tableView) {
        this.tableView = tableView;
    }
}
