/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;


import com.desktopapp.fx.commands.ICommand;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.control.fonticon.FontIconSize;
import com.desktopapp.fx.view.AppView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.input.KeyCodeCombination;

/**
 * Ribbon command
 *
 * @author Diwakar Choudhury
 */
public class ActionCommand extends Button {

    private ICommand command;

    private KeyCodeCombination keyCodeCombination;

    private boolean isChildAccelarator = true;

    private static final String RIBBON_MENU_STYLE_CLASS = "ribbon-command";
    private static final String BUTTTON_STYLE_CLASS = "button-command";


    public ActionCommand(ICommand command, String text, FontIcon fontIcon) {
        this(text, fontIcon, ContentDisplay.TOP);
        if(command != null) {
            this.command = command;
            setOnAction(event -> {
                this.command.executeCommand();
            });
        }
    }

    public ActionCommand(ICommand command, String text, FontIcon fontIcon, KeyCodeCombination keyCodeCombination) {
        this(text, fontIcon, ContentDisplay.TOP);
        if(command != null) {
            this.command = command;
            setOnAction(event -> {
                this.command.executeCommand();
            });
        }
        this.keyCodeCombination = keyCodeCombination;
        //this.getScene().getAccelerators().put(this.keyCodeCombination, this::fire);
    }

    public ActionCommand(String text, FontIcon fontIcon) {
        this(text, fontIcon, ContentDisplay.TOP);
    }

    public ActionCommand(String text, FontIcon fontIcon, ContentDisplay contentDisplay) {
        super(text, FontIconFactory.createIcon(fontIcon, FontIconSize.SM));
        setContentDisplay(contentDisplay);
        getStyleClass().add(RIBBON_MENU_STYLE_CLASS);
    }

    public ActionCommand(String text, KeyCodeCombination keyCodeCombination){
        super(text);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
        this.keyCodeCombination = keyCodeCombination;
    }

    public ActionCommand(String text, KeyCodeCombination keyCodeCombination, EventHandler<ActionEvent> action){
        super(text);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
        this.keyCodeCombination = keyCodeCombination;
        setOnAction(action);
    }

    public ActionCommand(String text, EventHandler<ActionEvent> action){
        super(text);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
        setOnAction(action);
    }

    public ActionCommand(String text){
        super(text);
        getStyleClass().add(BUTTTON_STYLE_CLASS);
    }

    public ICommand getCommand() {
        return command;
    }

    public void setCommand(ICommand command) {
        this.command = command;
    }

    public KeyCodeCombination getKeyCodeCombination() {
        return keyCodeCombination;
    }

    public void setKeyCodeCombination(KeyCodeCombination keyCodeCombination) {
        this.keyCodeCombination = keyCodeCombination;
    }


    public boolean isChildAccelarator() {
        return isChildAccelarator;
    }

    public void setChildAccelarator(boolean childAccelarator) {
        isChildAccelarator = childAccelarator;
    }
}
