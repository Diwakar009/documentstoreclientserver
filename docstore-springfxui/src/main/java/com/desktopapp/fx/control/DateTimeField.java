/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.control;

import com.desktopapp.fx.common.IGenericComponent;
import com.docstore.util.DateTimeUtil;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.StringConverter;
import javafx.util.converter.DateTimeStringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.function.UnaryOperator;

/**
 * Integer field control
 *
 * @see TextField
 * @see IntegerStringConverter
 * @see TextFormatter
 * @see UnaryOperator
 *
 * @author Diwakar C
 */
public class DateTimeField extends DatePicker implements IGenericComponent{

    FxDatePickerConverter converter;

    public DateTimeField() {
        super();
        setPrefWidth(110);
        converter = new FxDatePickerConverter();
    }

    @Override
    public void setDataValue(String value) {
        setValue(converter.fromString(value));
    }

    /**
     * Set Data Value using Date
     *
     * @param dataValue
     */
    public void setDataValue(Date dataValue){
        String dateString = DateTimeUtil.convertDateToUTCString(dataValue);
        setValue(converter.fromString(dateString));
    }

    @Override
    public void setAsReadOnly(boolean readOnly) {
         this.setDisabled(readOnly);
    }

    @Override
    public void destroyComponent() {

    }

    @Override
    public boolean isDataExchangeApplicable() {
        return true;
    }

    @Override
    public String getDataValue() {
        LocalDate date = getValue();
        String dateString = converter.toString(date);

        try {
            Date convertedDate = DateTimeUtil.getDateFromString(dateString, DateTimeUtil.FORMAT_dd_MM_yyyy);
            String convertedDateStr = DateTimeUtil.convertDateToUTCString(convertedDate);
            return convertedDateStr;
        }catch (ParseException e){

        }
        return dateString;
    }

    public Date getDataValueAsDate() {
        LocalDate date = getValue();
        String dateString = converter.toString(date);

        try {
            Date convertedDate = DateTimeUtil.getDateFromString(dateString, DateTimeUtil.FORMAT_dd_MM_yyyy);
            return convertedDate;
        }catch (ParseException e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean isComponentShowing() {
        return isShowing();
    }

    @Override
    public void clearDataValue() {
        this.setValue(null);
    }

    @Override
    public boolean isReadonly() {
        return super.isDisabled();
    }
}

/**
 * Custom date picker
 *
 */

class FxDatePickerConverter extends StringConverter{
    // Default Date Pattern
    private String pattern = "dd/MM/yyyy";
    //private String pattern = DateTimeUtil.UTC_FORMAT;
    // The Date Time Converter
    private DateTimeFormatter dtFormatter;

    public FxDatePickerConverter()
    {
        dtFormatter = DateTimeFormatter.ofPattern(pattern);
    }

    @Override
    public String toString(Object object) {
        return null;
    }

    public FxDatePickerConverter(String pattern)
    {
        this.pattern = pattern;
        dtFormatter = DateTimeFormatter.ofPattern(pattern);
    }

    // Change String to LocalDate
    public LocalDate fromString(String text)
    {
        LocalDate date = null;
        if (text != null && !text.trim().isEmpty()){
            date = LocalDate.parse(text, dtFormatter);
        }
        return date;
    }

    // Change LocalDate to String
    public String toString(LocalDate date)
    {
        String text = null;
        if (date != null){
            text = dtFormatter.format(date);
        }
        return text;
    }
}