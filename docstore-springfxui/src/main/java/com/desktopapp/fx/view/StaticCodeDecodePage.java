/*
 * Copyright (C) 2017 Cem Ikta 
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.view;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.control.MessageBox;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.mvc.AbstractDataPageView;
import com.desktopapp.fx.util.I18n;
import com.docstore.component.constants.CodeDecodeConstants;
import com.docstore.model.StaticCodeDecode;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import org.controlsfx.control.Notifications;
import org.docstore.domainobjs.json.StaticCodeDecodeDO;
import org.docstore.domainobjs.json.TableRowMapDO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * StaticCodeDecode page view
 *
 * @author diwakar009
 */
public class StaticCodeDecodePage extends AbstractDataPageView<StaticCodeDecodeDO> {

    @Override
    public List<TableColumn<StaticCodeDecodeDO, ?>> getTableViewColumns() {
        List<TableColumn<StaticCodeDecodeDO, ?>> columns = new ArrayList<>();

        TableColumn<StaticCodeDecodeDO, String> codeNameCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeName")
                .title(I18n.STATICCODEDECODE.getString("table.codeName"))
                .build();

        columns.add(codeNameCol);

        TableColumn<StaticCodeDecodeDO, String> languageCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("language")
                .title(I18n.STATICCODEDECODE.getString("table.language"))
                .decodeCode(CodeDecodeConstants.CD_LANGUAGE)
                .build();

        columns.add(languageCol);

        return columns;
    }

    @Override
    public StaticCodeDecodeDO mapTableRowtoDO(TableRowMapDO<String, String> tableRowMapDO) {

        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName(tableRowMapDO.getValue("codeName"));
        staticCodeDecodeDO.setCodeValue(tableRowMapDO.getValue("codeValue"));
        staticCodeDecodeDO.setCodeDesc(tableRowMapDO.getValue("codeDesc"));
        staticCodeDecodeDO.setLanguage(tableRowMapDO.getValue("language"));
        staticCodeDecodeDO.setCodeValueFilter(tableRowMapDO.getValue("codeValueFilter"));

        return staticCodeDecodeDO;
    }

    @Override
    public void onDelete(){
        StaticCodeDecodeDO staticCodeDecodeDO = (StaticCodeDecodeDO)getTableView().getSelectionModel().getSelectedItem();
        if(staticCodeDecodeDO != null) {
            ((StaticCodeDecodeController) getController()).onDeleteByCodeNameAndLanguage(staticCodeDecodeDO.getCodeName(), staticCodeDecodeDO.getLanguage());
        }
    }


    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STATICCODEDECODE;
    }

    @Override
    public String getTitle() {
        return I18n.STATICCODEDECODE.getString("title");
    }
}
