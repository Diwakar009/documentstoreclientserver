/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.view.stockmaintenance;


import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.TableColumnBuilder;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.controller.StockMaintenanceController;
import com.desktopapp.fx.mvc.AbstractDataPageView;
import com.desktopapp.fx.mvc.DataPageController;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.docstore.component.constants.CodeDecodeConstants;
import com.docstore.util.DateTimeUtil;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.*;
import javafx.util.Callback;
import org.docstore.domainobjs.json.DOPage;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.StockDetailsDO;
import org.docstore.domainobjs.json.sms.TransportDetailsDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.*;

/**
 * Stock Maintenance page view
 *
 * @author diwakar009
 */
public class StockDataSearchPage extends AbstractDataPageView<StockDetailsDO> {

    private final Logger logger = LoggerFactory.getLogger(StockDataSearchPage.class);

    private CodeDecodeField industryCodeDecode;
    private CodeDecodeField stockCodeDecode;
    private CodeDecodeField stockVareityCodeDecode;
    private CodeDecodeField transTypeCodeDecode;
    private DateTimeField startDate;
    private DateTimeField endDate;
    private TextFieldExt rstNumber;
    private TextFieldExt vehicleNumber;

    //:TODO Add Vehicle Number and RST Number

    private ActionCommand btnSearch;
    private ActionCommand btnClear;

    private List<IGenericComponent> inputComponents = new ArrayList<>();

    @Override
    protected Node renderCenter(){
        BorderPane dataPageBroderView = new BorderPane();
        dataPageBroderView.getStyleClass().addAll("search-data-page-view");
        dataPageBroderView.setTop(getScreenHeaderBar());
        dataPageBroderView.setCenter(buildCenter());
        dataPageBroderView.setBottom(buildButtom());
        return dataPageBroderView;
    }

    private Node buildCenter(){
        BorderPane searchPageCenterView = new BorderPane();
        searchPageCenterView.getStyleClass().addAll("search-data-page-center");
        Node filterPane = buildFilterPane();
        searchPageCenterView.setTop(filterPane);

        VBox tablePane = new VBox();
        tablePane.getChildren().addAll(getHeaderBar(),getTablePane());
        searchPageCenterView.setCenter(tablePane);
        return searchPageCenterView;
    }

    private Node buildFilterPane(){

        this.industryCodeDecode = new CodeDecodeField(CodeDecodeConstants.CD_INDUSTRY_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.industryCodeDecode.setPrefWidth(200);
        this.stockCodeDecode = new CodeDecodeField(CodeDecodeConstants.CD_STOCK_CODE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.stockVareityCodeDecode = new CodeDecodeField(CodeDecodeConstants.CD_STOCK_VARIETY,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.transTypeCodeDecode = new CodeDecodeField(CodeDecodeConstants.CD_TRANSACTION_TYPE,CodeDecodeConstants.DEFAULT_LANGUAGE);
        this.startDate = new DateTimeField();
        this.endDate = new DateTimeField();
        this.rstNumber= new TextFieldExt();
        this.vehicleNumber=new TextFieldExt();

        this.rstNumber.setPrefWidth(50);
        this.vehicleNumber.setPrefWidth(50);

        this.inputComponents.add(industryCodeDecode);
        this.inputComponents.add(stockCodeDecode);
        this.inputComponents.add(stockVareityCodeDecode);
        this.inputComponents.add(transTypeCodeDecode);
        this.inputComponents.add(startDate);
        this.inputComponents.add(rstNumber);
        this.inputComponents.add(vehicleNumber);


        this.stockCodeDecode.registerLinkedCodeDecodeField(stockVareityCodeDecode);
        this.stockCodeDecode.registerLinkedCodeDecodeField(transTypeCodeDecode);


        this.btnSearch= new ActionCommand("Search");
        this.btnClear= new ActionCommand("Clear");

        this.btnSearch.setTooltip(new Tooltip("Search"));
        this.btnSearch.getStyleClass().add("left-pill");
        this.btnSearch.setOnAction(event -> onSearch());

        this.btnClear.setTooltip(new Tooltip("Clear"));
        this.btnClear.getStyleClass().add("left-pill");
        this.btnClear.setOnAction(event -> onClear());

        BorderPane borderPane = new BorderPane();

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.setAlignment(Pos.CENTER);

        formPane.add(new Label("Mill Name"), 0, 0);
        formPane.add(industryCodeDecode.asControl(), 1, 0);

        formPane.add(new Label("Stock Name"), 0, 1);
        formPane.add(stockCodeDecode.asControl(), 1, 1);

        formPane.add(new Label("Stock Variety"), 2, 1);
        formPane.add(stockVareityCodeDecode.asControl(), 3, 1);

        formPane.add(new Label("Transaction Type"), 0, 2);
        formPane.add(transTypeCodeDecode.asControl(), 1, 2);

        formPane.add(new Label("Transaction Date (DD/MM/YYYY)"), 2, 2);
        HBox startDateEndDateHBox = new HBox();
        startDateEndDateHBox.getChildren().add(startDate);
        startDateEndDateHBox.getChildren().add(new Label(" __ "));
        startDateEndDateHBox.getChildren().add(endDate);
        formPane.add(startDateEndDateHBox,3 , 2);

        formPane.add(new Label("RST Number"), 0, 3);
        formPane.add(rstNumber, 1, 3);

        formPane.add(new Label("Vehicle Number"), 2, 3);
        formPane.add(vehicleNumber, 3, 3);

        HBox buttonBox = new HBox();
        buttonBox.setStyle("-fx-pref-height: 50px;");
        buttonBox.getChildren().add(btnSearch);
        buttonBox.getChildren().add(btnClear);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(40);

        borderPane.setTop(formPane);
        borderPane.setCenter(buttonBox);
        return borderPane;
    }

    /**
     * Get Table Data.
     *
     * @return
     */
    @Override
    public DOPage<TableRowMapDO<String,String>> getData(){
        PageDetails details = new PageDetails();
        details.setStart(this.pager.getStartPosition());
        details.setEnd(this.pager.getEndPosition());
        details.setPageNumber(this.pager.getCurrentPageIndex() - 1);
        details.setSize(this.pager.getItemsPerPage());

        String industryCode = this.industryCodeDecode.getDataValue();
        String stockCode = this.stockCodeDecode.getDataValue();
        String stockVarietyCode = this.stockVareityCodeDecode.getDataValue();
        String transactionType = this.transTypeCodeDecode.getDataValue();
        Date transStartDate = this.startDate.getDataValueAsDate();
        Date transEndDate = this.endDate.getDataValueAsDate();
        String rstNumber = this.rstNumber.getDataValue();
        String vehicleNumber = this.vehicleNumber.getDataValue();


        return ((StockMaintenanceController)getController()).getData(industryCode,stockCode,stockVarietyCode,transactionType,transStartDate,transEndDate,rstNumber,vehicleNumber,details);
    }

    /**
     * Event on click of onSearch
     *
     */
    public void onSearch(){
       this.refreshData();
    }

    /**
     * Event on click onClear
     *
     * Clear the filter criteria
     *
     */
    public void onClear(){
        this.inputComponents.forEach(
                component -> {component.clearDataValue();
                });
    }

    protected HBox getScreenHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("search-data-page-header");

        Label title = ViewHelpers.createFontIconLabel(getTitle(), getFontIcon(), "");
        title.getStyleClass().add("search-data-page-title");

        /*progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(50, 50);*/

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        headerBar.getChildren().addAll(title, spacer);
        return headerBar;
    }

    protected HBox getHeaderBar() {
        logger.debug("get data page header");
        HBox headerBar = new HBox();
        //headerBar.getStyleClass().add("data-page-header");

        Label title = ViewHelpers.createFontIconLabel("Stock Transaction's", getFontIcon(), "");
        title.getStyleClass().add("page-title");

        progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(50, 50);

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        // actions
        btnAddNew = new Button("", FontIconFactory.createIcon(AppFontIcons.ADD_NEW));
        btnAddNew.setTooltip(new Tooltip(I18n.COMMON.getString("action.addNew")));
        btnAddNew.getStyleClass().add("left-pill");
        btnAddNew.setOnAction(event -> onAddNew());

        btnEdit = new Button("", FontIconFactory.createIcon(AppFontIcons.EDIT));
        btnEdit.setTooltip(new Tooltip(I18n.COMMON.getString("action.edit")));
        btnEdit.getStyleClass().add("right-pill");
        btnEdit.setOnAction(event -> onEdit());
        HBox.setMargin(btnEdit, new Insets(0, 10, 0, 0));

        btnDelete = new Button("", FontIconFactory.createIcon(AppFontIcons.DELETE));
        btnDelete.setTooltip(new Tooltip(I18n.COMMON.getString("action.delete")));
        btnDelete.setOnAction(event -> onDelete());
        HBox.setMargin(btnDelete, new Insets(0, 10, 0, 0));

        headerBar.getChildren().addAll(title, progressIndicator, spacer, btnAddNew, btnEdit, btnDelete);

        if (hasPrintActions()) {
            btnPrintPreview = new Button("", FontIconFactory.createIcon(AppFontIcons.PRINT_PREVIEW));
            btnPrintPreview.setTooltip(new Tooltip(I18n.COMMON.getString("action.printPreview")));
            btnPrintPreview.getStyleClass().add("left-pill");
            btnPrintPreview.setOnAction(event -> onPrintPreview());

            btnPrint = new Button("", FontIconFactory.createIcon(AppFontIcons.PRINT));
            btnPrint.setTooltip(new Tooltip(I18n.COMMON.getString("action.print")));
            btnPrint.getStyleClass().add("center-pill");
            btnPrint.setOnAction(event -> onPrint());

            btnPdf = new Button("", FontIconFactory.createIcon(AppFontIcons.PDF));
            btnPdf.setTooltip(new Tooltip(I18n.COMMON.getString("action.pdf")));
            btnPdf.getStyleClass().add("right-pill");
            btnPdf.setOnAction(event -> onPdf());
            HBox.setMargin(btnPdf, new Insets(0, 10, 0, 0));

            headerBar.getChildren().addAll(btnPrintPreview, btnPrint, btnPdf);
        }

        /*// search field
        searchField = new SearchField(300);
        searchField.textProperty().addListener((observable, oldValue, newValue) -> {
            pager.setCurrentPageIndex(1);
            refreshData();
        });
        HBox.setMargin(searchField, new Insets(0, 0, 0, 100));
        headerBar.getChildren().add(searchField);*/

        return headerBar;
    }

    @Override
    public IAccelerator getAccelerator() {
        return new IAccelerator() {
            @Override
            public Map<String, Map<KeyCodeCombination, Runnable>> getAccelerators() {
                Map<String,Map<KeyCodeCombination, Runnable>> combinations = new HashMap<String,Map<KeyCodeCombination, Runnable>>();
                Map<KeyCodeCombination,Runnable> childAccelerators = new HashMap<KeyCodeCombination,Runnable>();
                combinations.put(IAccelerator.CHILD_ACCELERATORS,childAccelerators);
                combinations.put(IAccelerator.PARENT_ACCELERATORS,new HashMap<KeyCodeCombination,Runnable>());
                return combinations;
            }
        };
    }

    @Override
    public List<TableColumn<StockDetailsDO, ?>> getTableViewColumns() {
        List<TableColumn<StockDetailsDO, ?>> columns = new ArrayList<>();

        TableColumn<StockDetailsDO, Date> transactionDateCol = TableColumnBuilder.<StockDetailsDO, Date> create()
                .fieldName("transactionDate")
                .title("Transaction Date")
                .dataType(TableColumnBuilder.DATATYPE_DATE)
                .build();

        columns.add(transactionDateCol);

        TableColumn<StockDetailsDO, String> industryCodeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("industryCode")
                .title("Industry Code")
                .decodeCode(CodeDecodeConstants.CD_INDUSTRY_CODE)
                .build();

        columns.add(industryCodeCol);

        TableColumn<StockDetailsDO, String> stockCodeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("stockCode")
                .title("Stock")
                .decodeCode(CodeDecodeConstants.CD_STOCK_CODE)
                .build();

        columns.add(stockCodeCol);

        TableColumn<StockDetailsDO, String> stockVarietyCodeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("stockVarietyCode")
                .title("Stock Varierty")
                .decodeCode(CodeDecodeConstants.CD_STOCK_VARIETY)
                .build();

        columns.add(stockVarietyCodeCol);

        TableColumn<StockDetailsDO, String> transactionTypeCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .fieldName("transactionType")
                .title("Transaction Type")
                .decodeCode(CodeDecodeConstants.CD_TRANSACTION_TYPE)
                .build();

        columns.add(transactionTypeCol);

        TableColumn<StockDetailsDO, Long> noOfPacketsCol = TableColumnBuilder.<StockDetailsDO, Long> create()
                .fieldName("noOfPackets")
                .title("No of Packets")
                .dataType(TableColumnBuilder.DATATYPE_NO_OF_PKT)
                .build();

        columns.add(noOfPacketsCol);

        TableColumn<StockDetailsDO, Double> weightmentCol = TableColumnBuilder.<StockDetailsDO, Double> create()
                .fieldName("totalWeightment")
                .title("Weightment")
                .dataType(TableColumnBuilder.DATATYPE_WEIGHMENT)
                .build();

        columns.add(weightmentCol);

        TableColumn<StockDetailsDO, String> rstNumberCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .title("RST Number")
                .build();

        rstNumberCol.setCellValueFactory(cellvaluefac -> {
                if(cellvaluefac.getValue().getTransportDetailsDO() != null) {
                    return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTransportDetailsDO().getRstNumber());
                }
                return new ReadOnlyObjectWrapper("");
        });


        columns.add(rstNumberCol);

        TableColumn<StockDetailsDO, String> vehicleNumberCol = TableColumnBuilder.<StockDetailsDO, String> create()
                .title("Vehicle Number")
                .build();

        vehicleNumberCol.setCellValueFactory(cellvaluefac -> {
                if(cellvaluefac.getValue().getTransportDetailsDO() != null) {
                    return new ReadOnlyObjectWrapper(cellvaluefac.getValue().getTransportDetailsDO().getVehicleNumber());
                }
                return new ReadOnlyObjectWrapper("");
        });

        columns.add(vehicleNumberCol);

        TableColumn<StockDetailsDO, Long> idCol = TableColumnBuilder.<StockDetailsDO, Long> create()
                .fieldName("id")
                .title("Id")
                .visible(false)
                .prefWidth(20)
                .build();

        columns.add(idCol);

        return columns;
    }

    @Override
    public void onAddNew() {
        super.onAddNew(); // :TODO
    }

    @Override
    public void onEdit() {
        super.onEdit(); // :TODO
    }

    @Override
    public void onDelete() {
        super.onDelete(); // :TODO
    }

    @Override
    public StockDetailsDO mapTableRowtoDO(TableRowMapDO<String, String> tableRowMapDO) {
        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setId(Long.parseLong(tableRowMapDO.getValue("id")));
        stockDetailsDO.setIndustryCode(tableRowMapDO.getValue("industryCode"));
        stockDetailsDO.setStockCode(tableRowMapDO.getValue("stockCode"));
        stockDetailsDO.setStockVarietyCode(tableRowMapDO.getValue("stockVarietyCode"));
        stockDetailsDO.setNoOfPackets(Long.parseLong(tableRowMapDO.getValue("numberOfPackets")));
        stockDetailsDO.setTotalWeightment(Double.parseDouble(tableRowMapDO.getValue("weightment")));
        stockDetailsDO.setTransactionType(tableRowMapDO.getValue("transactionType"));

        if(tableRowMapDO.getValue("rstNumber") != null || tableRowMapDO.getValue("vehicleNumber") != null){
            stockDetailsDO.setTransportDetailsDO(new TransportDetailsDO());
            stockDetailsDO.getTransportDetailsDO().setRstNumber(tableRowMapDO.getValue("rstNumber"));
            stockDetailsDO.getTransportDetailsDO().setVehicleNumber(tableRowMapDO.getValue("vehicleNumber"));
        }

        try {
            Date transactionDate = DateTimeUtil.convertUTCStringToDate(tableRowMapDO.getValue("tranactionDate"));
            stockDetailsDO.setTransactionDate(transactionDate);
        }catch (ParseException e){
            logger.error("Transaction date parse error ' " + tableRowMapDO.getValue("tranactionDate") + "'");
        }

        return stockDetailsDO;
    }


    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STOCKMANAGEMENT;
    }

    @Override
    public String getTitle() {
        return "Stock Maintenance";
    }
}
