package com.desktopapp.fx.view;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.common.ITableDataViewBehaviour;
import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.fonticon.FontIcon;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.mvc.AbstractFormView;
import com.desktopapp.fx.mvc.DataPageController;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import com.docstore.component.constants.DocStoreConstants;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.*;
import org.controlsfx.validation.Validator;
import org.docstore.domainobjs.json.DOPage;
import org.docstore.domainobjs.json.StaticCodeDecodeDO;
import org.docstore.domainobjs.json.TableRowMapDO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 3/3/2019.
 */
public class StaticCodeDecodeForm extends AbstractFormView<StaticCodeDecodeDO> {

    private StaticCodeDecodeDO staticCodeDecodeDO;
    private TextFieldExt tfCodeDesc;
    private TextFieldExt tfCodeName;
    private TextFieldExt tfCodeValueFilter;
    private TextFieldExt tfCodeValue;
    private CodeDecodeField cdLanguage;
    private TableDataView<StaticCodeDecodeDO> tableDataView;

    public StaticCodeDecodeForm(DataPageController<StaticCodeDecodeDO> controller, StaticCodeDecodeDO staticCodeDecodeDO) {
        super(controller);
        this.staticCodeDecodeDO = staticCodeDecodeDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }

    public StaticCodeDecodeForm(DataPageController<StaticCodeDecodeDO> controller, StaticCodeDecodeDO staticCodeDecodeDO,StackedScreen stackedScreen) {
        super(controller,stackedScreen);
        this.staticCodeDecodeDO = staticCodeDecodeDO;
        loadLatestEntity();
        this.getLblformTitle().setText(getTitle());
        pop();
    }


    @Override
    public Node buildFormView() {

        tfCodeName = ViewHelpers.createTextFieldExt(30, 200);
        getValidationSupport().registerValidator(tfCodeName, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        tfCodeValueFilter = ViewHelpers.createTextFieldExt(5, 200);

        tfCodeValue = ViewHelpers.createTextFieldExt(5, 200);
        getValidationSupport().registerValidator(tfCodeValue, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));

        cdLanguage = ViewHelpers.createCodeDecodeField(DocStoreConstants.CD_LANGUAGE,"EN_US",200);

        tfCodeDesc = ViewHelpers.createTextFieldExt(100, 200);
        getValidationSupport().registerValidator(tfCodeDesc, true,
                Validator.createEmptyValidator(I18n.COMMON.getString("validation.required")));


        registerComponents(tfCodeName,tfCodeValueFilter,tfCodeValue,cdLanguage,tfCodeDesc);

        GridPane formPane = new GridPane();
        formPane.setPadding(new Insets(20, 30, 10, 30));
        formPane.setHgap(10);
        formPane.setVgap(10);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeName")), 0, 0);
        formPane.add(tfCodeName, 1, 0);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValue")), 0, 1);
        formPane.add(tfCodeValue, 1, 1);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeValueFilter")), 0, 2);
        formPane.add(tfCodeValueFilter, 1, 2);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.codeDesc")), 0, 3);
        formPane.add(tfCodeDesc, 1, 3);

        formPane.add(new Label(I18n.STATICCODEDECODE.getString("form.language")), 0, 4);
        formPane.add(cdLanguage.asControl(), 1, 4);

        tableDataView = new TableDataView(new StaticCodeDecodeTableBehaviour(this));

        VBox vbox = new VBox();

        HBox formPaneHBox = new HBox();
        formPaneHBox.getChildren().add(formPane);
        formPaneHBox.setAlignment(Pos.CENTER);

        HBox tablePaneHBox = new HBox();
        tablePaneHBox.getChildren().add(tableDataView);
        HBox.setHgrow(tableDataView,Priority.ALWAYS);

        vbox.getChildren().add(formPaneHBox);
        vbox.getChildren().add(tablePaneHBox);

        return vbox;
    }

    @Override
    public StaticCodeDecodeDO getCurrentEntity() {
        return staticCodeDecodeDO;
    }

    @Override
    public void loadLatestEntity() {

    }

    @Override
    public void pop() {

        if( !staticCodeDecodeDO.isNew() ){
            tfCodeName.setAsReadOnly(true);
            cdLanguage.setAsReadOnly(true);
        }

        setDOToControls(staticCodeDecodeDO);
        tableDataView.refreshData(); // refresh the table data
    }

    public void setDOToControls(StaticCodeDecodeDO staticCodeDecodeDO){
        if(staticCodeDecodeDO != null) {
            tfCodeName.setDataValue(staticCodeDecodeDO.getCodeName());
            tfCodeValue.setDataValue(staticCodeDecodeDO.getCodeValue());
            tfCodeDesc.setDataValue(staticCodeDecodeDO.getCodeDesc());
            tfCodeValueFilter.setDataValue(staticCodeDecodeDO.getCodeValueFilter());
            cdLanguage.setDataValue(staticCodeDecodeDO.getLanguage());
        }
    }

    @Override
    public void onDelete(){
        ((StaticCodeDecodeController)getController()).onDelete((StaticCodeDecodeDO) tableDataView.getTableView().getSelectionModel().getSelectedItem());
        refreshForm();
    }

    @Override
    public void push() {
        staticCodeDecodeDO.setCodeName(tfCodeName.getDataValue());
        staticCodeDecodeDO.setCodeValue(tfCodeValue.getDataValue());
        staticCodeDecodeDO.setCodeDesc(tfCodeDesc.getDataValue());
        staticCodeDecodeDO.setLanguage(cdLanguage.getDataValue());
        staticCodeDecodeDO.setCodeValueFilter(tfCodeValueFilter.getDataValue());

        if (staticCodeDecodeDO.isNew()) {
            staticCodeDecodeDO.setCreatedBy(DesktopFxApplication.getLoginSession().getUserName());
        } else {
            staticCodeDecodeDO.setUpdatedBy(DesktopFxApplication.getLoginSession().getUserName());
        }
    }

    @Override
    public void refreshForm() {
        getTableDataView().refreshData();
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STATICCODEDECODE;
    }

    @Override
    public String getTitle() {
        return staticCodeDecodeDO.isNew() ? I18n.STATICCODEDECODE.getString("form.newTitle") : I18n.STATICCODEDECODE.getString("form.editTitle");
    }

    @Override
    public Node asNode() {
        return this;
    }

    @Override
    public void addMoreActionCommands(List<ActionCommand> commands) {

    }

    public StaticCodeDecodeDO getStaticCodeDecodeDO() {
        return staticCodeDecodeDO;
    }

    public void setStaticCodeDecodeDO(StaticCodeDecodeDO staticCodeDecodeDO) {
        this.staticCodeDecodeDO = staticCodeDecodeDO;
    }

    public TableDataView<StaticCodeDecodeDO> getTableDataView() {
        return tableDataView;
    }

    public void setTableDataView(TableDataView<StaticCodeDecodeDO> tableDataView) {
        this.tableDataView = tableDataView;
    }
}

/**
 * Static code decode Table properties and behaviour
 */
 class StaticCodeDecodeTableBehaviour implements ITableDataViewBehaviour<StaticCodeDecodeDO>{

    private  StaticCodeDecodeForm form;

    public StaticCodeDecodeTableBehaviour(StaticCodeDecodeForm form) {
        this.form = form;
    }

    @Override
    public List<TableColumn<StaticCodeDecodeDO, ?>> getTableViewColumns() {
        List<TableColumn<StaticCodeDecodeDO, ?>> columns = new ArrayList<>();

        TableColumn<StaticCodeDecodeDO, String> codeNameCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeName")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.codeName"))
                .build();

        columns.add(codeNameCol);

        TableColumn<StaticCodeDecodeDO, String> languageCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("language")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.language"))
                .build();

        columns.add(languageCol);

        TableColumn<StaticCodeDecodeDO, String> codeValueFilterCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeValueFilter")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.codeValueFilter"))
                .build();

        columns.add(codeValueFilterCol);

        TableColumn<StaticCodeDecodeDO, String> codeValueCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeValue")
                .prefWidth(100)
                .title(I18n.STATICCODEDECODE.getString("table.codeValue"))
                .build();
        columns.add(codeValueCol);

        TableColumn<StaticCodeDecodeDO, String> codeDescCol = TableColumnBuilder.<StaticCodeDecodeDO, String> create()
                .fieldName("codeDesc")
                .title(I18n.STATICCODEDECODE.getString("table.codeDesc"))
                .build();

        columns.add(codeDescCol);
        return columns;
    }

    @Override
    public DOPage<TableRowMapDO<String, String>> getData(String filter, PageDetails details) {
        return ((StaticCodeDecodeController)getForm().getController()).listByCodeNameNLang(form.getStaticCodeDecodeDO().getCodeName(), filter, form.getStaticCodeDecodeDO().getLanguage(), details);
    }

    @Override
    public StaticCodeDecodeDO mapTableRowToDO(TableRowMapDO<String,String> tableRowMapDO) {
        StaticCodeDecodeDO staticCodeDecodeDO = new StaticCodeDecodeDO();
        staticCodeDecodeDO.setCodeName(tableRowMapDO.getValue("codeName"));
        staticCodeDecodeDO.setCodeValue(tableRowMapDO.getValue("codeValue"));
        staticCodeDecodeDO.setCodeDesc(tableRowMapDO.getValue("codeDesc"));
        staticCodeDecodeDO.setLanguage(tableRowMapDO.getValue("language"));
        staticCodeDecodeDO.setCodeValueFilter(tableRowMapDO.getValue("codeValueFilter"));
        return staticCodeDecodeDO;
    }

    @Override
    public void onSelectedItemChanged() {
        StaticCodeDecodeDO selectedItem = (StaticCodeDecodeDO) getForm().getTableDataView().getTableView().getSelectionModel().getSelectedItem();
        if(selectedItem != null) {
            getForm().setDOToControls(selectedItem);
        }
    }

    @Override
    public Node asNode() {
        return null;
    }

    @Override
    public FontIcon getFontIcon() {
        return AppFontIcons.STATICCODEDECODE;
    }

    @Override
    public String getTitle() {
        return I18n.STATICCODEDECODE.getString("title");
    }

    public StaticCodeDecodeForm getForm() {
        return form;
    }

    public void setForm(StaticCodeDecodeForm form) {
        this.form = form;
    }



}
