package com.desktopapp.fx;

import com.desktopapp.fx.app.AppFeatures;
import com.desktopapp.fx.app.AppTheme;
import com.desktopapp.fx.common.StageViewBean;
import com.desktopapp.fx.control.AppHelp;
import com.desktopapp.fx.manager.IBootManager;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.session.LoginSession;
import com.desktopapp.fx.util.I18n;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Main class for starting the application
 *
 */
@SpringBootApplication
@ComponentScan({"com.desktopapp.fx.controller",
		"com.desktopapp.fx.manager",
		"com.desktopapp.service",
		"com.docstore.jpa.spring.extended"})
public class DesktopFxApplication extends Application {

	private final Logger logger = LoggerFactory.getLogger(DesktopFxApplication.class);
	private static DesktopFxApplication desktopFxApplication;
	//private StageViewBean stageView;

	/**
	 * login session per instance
	 *
	 */
	private static LoginSession loginSession;

	/**
	 * Product startup initialization of spring objects
	 *
	 */
	private IBootManager productBootManager;


	public static void main(String[] args) {
		// Initilize the spring application context and assign it to object factory.
		// Spring boot annotation is must.

		ApplicationContext context = SpringApplication.run(DesktopFxApplication.class, args);

		ObjectFactory.setContext(context);
		// english
		Locale.setDefault(new Locale("en", "GB"));
		// german
		//Locale.setDefault(new Locale("de", "DE"));
		// turkish
		//Locale.setDefault(new Locale("tr", "TR"));
		launch(args);
	}

	@Override
	public void init() throws Exception {
		logger.info("init application");
		loginSession = new LoginSession();
		productBootManager= IBootManager.getProductBootManager();
	}

	@Override
	public void start(Stage initStage) throws Exception {
		logger.info("start application");
		desktopFxApplication = this;

		Map<String,Object> initialStageMap = new HashMap<String,Object>();
		initialStageMap.put(IBootManager.INITIAL_STAGE,initStage);


		productBootManager.startApplication(initialStageMap);
	}

	public void showAppHelp() {
		new AppHelp.Builder()
				.title(I18n.COMMON.getString("appHelp.title"))
				.iconPath(AppFeatures.APP_ICON)
				.helpHtmlPath("/help/help.html")
				.stylesheet(getCurrentAppTheme().getThemeStyle())
				.build()
				.show();
	}

	public static AppTheme getCurrentAppTheme() {
		return AppTheme.valueOf(getLoginSession().getPreferences().getAppTheme().toUpperCase());
	}

	public void changeAppTheme(AppTheme appTheme) {
		getLoginSession().getStageView().getScene().getStylesheets().clear();
		getLoginSession().getStageView().getScene().getStylesheets().add(getClass().getResource(appTheme.getThemeStyle()).toExternalForm());
	}

	/**
	 * Gets application instance.
	 *
	 * @return app instance
	 */
	public static DesktopFxApplication get() {
		return desktopFxApplication;
	}

	public Stage getMainStage() {
		return getLoginSession().getStageView().getStage();
	}


	public static LoginSession getLoginSession() {
		return loginSession;
	}

	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}


	public void onAppExit(WindowEvent windowEvent) {
		Map<String,Object> finalStageMap = new HashMap<String,Object>();
		finalStageMap.put(IBootManager.WINDOW_EVENT,windowEvent);
		productBootManager.exitApplication(finalStageMap);
	}
}
