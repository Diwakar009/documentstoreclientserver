package com.desktopapp.fx.manager;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.app.SplashTask;
import com.desktopapp.fx.common.InitCompletionHandler;
import com.desktopapp.fx.control.Splash;
import javafx.animation.FadeTransition;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.springframework.stereotype.Component;

/**
 * Created by diwakar009 on 13/12/2018.
 */
@Component
public class SplashManager {

    public void startSplash(Stage initStage,InitCompletionHandler initCompletionHandler){
        final SplashTask splashTask = new SplashTask();
        showSplash(initStage, splashTask, initCompletionHandler);
        new Thread(splashTask).start();
    }

    private void showSplash(final Stage initStage, Task<?> task, InitCompletionHandler initCompletionHandler) {
        Splash splash = new Splash(AppFontIcons.APP);
        splash.getProgressText().textProperty().bind(task.messageProperty());
        splash.getLoadProgress().progressProperty().bind(task.progressProperty());

        task.stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                splash.getLoadProgress().progressProperty().unbind();
                splash.getLoadProgress().setProgress(1);
                initStage.toFront();

                FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2), splash);
                fadeSplash.setFromValue(1.0);
                fadeSplash.setToValue(0.0);
                fadeSplash.setOnFinished(actionEvent -> initStage.hide());
                fadeSplash.play();

                initCompletionHandler.complete();
            }
        });

        Scene splashScene = new Scene(splash);
        splashScene.getStylesheets().add(getClass().getResource(DesktopFxApplication.get().getCurrentAppTheme().getThemeStyle()).toExternalForm());

        initStage.initStyle(StageStyle.UNDECORATED);
        initStage.setScene(splashScene);
        initStage.show();
    }


}
