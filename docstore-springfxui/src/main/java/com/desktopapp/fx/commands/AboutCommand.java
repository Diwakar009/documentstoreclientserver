package com.desktopapp.fx.commands;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.control.AboutDialog;
import com.desktopapp.fx.util.I18n;
import org.springframework.stereotype.Component;

/**
 * Created by diwakar009 on 24/12/2018.
 */
@Component
public class AboutCommand implements ICommand{

    @Override
    public void executeCommand() {
        onOpenAbout();
    }

    private void onOpenAbout() {
      /*  logger.info("On open about dialog");*/
        new AboutDialog.Builder()
                .owner(DesktopFxApplication.get().getMainStage())
                .title(I18n.COMMON.getString("aboutDialog.title"))
                .appIcon(AppFontIcons.APP)
                .appName(I18n.COMMON.getString("aboutDialog.appName"))
                .appVersion(I18n.COMMON.getString("aboutDialog.appVersion"))
                .appCopyright(I18n.COMMON.getString("aboutDialog.appCopyright"))
                .appHomepage("http://cemikta.com")
                .build()
                .showAndWait();
    }
}
