package com.desktopapp.fx.commands;

import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.view.AppView;
import javafx.scene.Node;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class DefaultCommand implements ICommand {

    private AppView view;

    public DefaultCommand(AppView view) {
        this.view = view;
    }

    @Override
    public void executeCommand() {

    }
}
