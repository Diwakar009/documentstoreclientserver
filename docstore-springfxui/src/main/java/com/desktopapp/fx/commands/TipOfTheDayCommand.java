package com.desktopapp.fx.commands;

import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class TipOfTheDayCommand implements ICommand{

    private AppView view;

    public TipOfTheDayCommand(AppView view) {
        this.view = view;
    }

    @Override
    public void executeCommand() {
        onOpenTipOfTheDay();
    }

    private void onOpenTipOfTheDay() {
        this.view.getApplicationViewManager().showTipOfTheDay();
    }
}
