package com.desktopapp.fx.commands;

import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.view.ShortcutsDialog;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class KeyboardShortcutsCommand implements ICommand {

    @Override
    public void executeCommand() {
        onOpenKeyboardShortcuts();
    }

    private void onOpenKeyboardShortcuts() {
        new ShortcutsDialog(DesktopFxApplication.get().getMainStage()).showAndWait();
    }
}
