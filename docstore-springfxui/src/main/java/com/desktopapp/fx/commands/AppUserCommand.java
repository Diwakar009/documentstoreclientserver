package com.desktopapp.fx.commands;

import com.desktopapp.fx.controller.AppUserController;
import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class AppUserCommand implements ICommand {

    private AppView view;

    private AppUserController appUserController;

    public AppUserCommand(AppView view) {
        this.view = view;
        this.appUserController = (AppUserController) ObjectFactory.getControllerObject(ObjectFactory.APP_USER_CTRL);
    }

    @Override
    public void executeCommand() {
        this.view.addPageToCenter(appUserController.getDataPageView());
    }
}
