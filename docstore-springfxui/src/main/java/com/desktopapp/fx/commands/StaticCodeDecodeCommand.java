package com.desktopapp.fx.commands;

import com.desktopapp.fx.controller.StaticCodeDecodeController;
import com.desktopapp.fx.objectfactory.ObjectFactory;
import com.desktopapp.fx.view.AppView;

/**
 * Created by diwakar009 on 24/12/2018.
 */
public class StaticCodeDecodeCommand implements ICommand {

    private AppView view;

    private StaticCodeDecodeController staticCodeDecodeController;

    public StaticCodeDecodeCommand(AppView view) {
        this.view = view;
        this.staticCodeDecodeController = (StaticCodeDecodeController) ObjectFactory.getControllerObject(ObjectFactory.STATIC_CODE_DECODE_CTRL);
    }

    @Override
    public void executeCommand() {
        this.view.addPageToCenter(staticCodeDecodeController.getDataPageView());
    }


}
