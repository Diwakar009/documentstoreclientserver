package com.desktopapp.fx.commands.factory;

import com.desktopapp.fx.commands.AppUserCommand;
import com.desktopapp.fx.commands.ICommand;
import javafx.scene.Node;

/**
 * Created by diwakar009 on 10/3/2019.
 */
public interface ICommandFactory {
    public static String CMD_ABOUT = "AboutCommand";
    public static String CMD_EXIT = "ExitCommand";
    public static String CMD_HELP = "HelpCommand";
    public static String CMD_KEYBOARDSHORTCUT = "KeyBoardShortCutCommand";
    public static String CMD_PREFERENCE = "PreferenceCommand";
    public static String CMD_STATUSBAR = "StatusBarCommand";
    public static String CMD_THEMES = "ThemesCommand";
    public static String CMD_TIPOFTHEDAY = "TipOfTheDayCommand";

    public static String CMD_APPUSER = "AppUserCommand";

    public static String CMD_STATICCODEDECODE = "StaticCodeDecodeCommand";
    public static String CMD_STOCKMAINTENANCE="StockMaintenanceCommand";

    public static String CMD_DEFAULT = "DefaultCommand";

    public ICommand getCommand(Node node, String commandName);
}

