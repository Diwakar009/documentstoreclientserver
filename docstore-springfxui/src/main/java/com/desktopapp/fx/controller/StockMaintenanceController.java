/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.fx.controller;


import com.desktopapp.fx.common.PageDetails;
import com.desktopapp.fx.mvc.AbstractDataPageController;
import com.desktopapp.fx.mvc.DataPageView;
import com.desktopapp.fx.view.stockmaintenance.StockDataSearchPage;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import org.docstore.domainobjs.json.DOPage;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.StockDetailsDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.desktopapp.service.QueryParameter.with;

/**
 * Stock Maintenance controller
 *
 * @author diwakar009
 */
@Component
public class StockMaintenanceController extends AbstractDataPageController<StockDetailsDO> {

    private final Logger logger = LoggerFactory.getLogger(StockMaintenanceController.class);

    @Autowired
    private StockDetailsService stockDetailsService;

    @Override
    protected AbstractService<StockDetailsDO> createService() {
        return stockDetailsService;
    }

    @Override
    protected DataPageView<StockDetailsDO> createDataPageView() {
        return new StockDataSearchPage();
    }


    /**
     * Get stock details data
     *
     * @param industryCode
     * @param stockCode
     * @param stockVarietyCode
     * @param transactionType
     * @param transStartDate
     * @param transEndDate
     * @param pageDetails
     * @return
     */

    public DOPage<TableRowMapDO<String,String>> getData(String industryCode,
                                                        String stockCode,
                                                        String stockVarietyCode,
                                                        String transactionType,
                                                        Date transStartDate,
                                                        Date transEndDate,
                                                        String rstNumber,
                                                        String vehicleNumber,
                                                        PageDetails pageDetails) {

        boolean filterEnabled = false;

        if(!"".equals(industryCode)
                || !"".equals(stockCode)
                || !"".equals(stockVarietyCode)
                || !"".equals(transactionType)
                || !"".equals(rstNumber)
                || !"".equals(vehicleNumber)
                || transStartDate != null
                || transEndDate != null){
            filterEnabled = true;
        }

        if (!filterEnabled) {
            return getService().listAll(with("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).parameters());
        } else {
            return getService().listByFilter(with("industryCode", industryCode).
                    and("stockCode", stockCode).
                    and("stockVarietyCode", stockVarietyCode).
                    and("transactionType", transactionType).
                    and("rstNumber", rstNumber).
                    and("vehicleNumber", vehicleNumber).
                    and("transStartDate", transStartDate).
                    and("transEndDate", transEndDate).
                    and("page", String.valueOf(pageDetails.getPageNumber())).
                    and("size", String.valueOf(pageDetails.getSize())).
                    and("start",pageDetails.getStart()).
                    and("end",pageDetails.getEnd()).
                    parameters());

        }
    }

    @Override
    public void openFormView(StockDetailsDO stockDetailsDO) {
        //new StaticCodeDecodeForm(this, staticCodeDecodeDO);
    }

    @Override
    public void openFormViewAtCenter(StockDetailsDO stockDetailsDO) {
        //new StaticCodeDecodeForm(this, stockDetailsDO,this.getMultScreenPane());
    }

    @Override
    public void onAddNew() {
    }

    @Override
    public void onAddNewAtCenter() {
        //new StaticCodeDecodeForm(this, new StaticCodeDecodeDO(),(StackedScreen) this.getDataPageView().getStackedScreen());
    }

    @Override
    public String getName() {
        return "StockMaintenanceController";
    }
}