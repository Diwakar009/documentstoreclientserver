/*
 * Copyright (C) 2019 R Diwakar C
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package com.desktopapp.fx.mvc;


import com.desktopapp.fx.DesktopFxApplication;
import com.desktopapp.fx.app.AppKeyCode;
import com.desktopapp.fx.common.IAccelerator;
import com.desktopapp.fx.common.IGenericComponent;
import com.desktopapp.fx.common.IStackedScreenProperties;
import com.desktopapp.fx.control.*;
import com.desktopapp.fx.control.multiscreen.StackedScreen;
import com.desktopapp.fx.util.I18n;
import com.desktopapp.fx.util.ViewHelpers;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import org.controlsfx.control.Notifications;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.docstore.domainobjs.json.BaseDomainObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Abstract form view for data editing in {@link Dialog} forms.
 *
 * @param <T> entity
 * @see DataPageController
 *
 * @author R Diwakar C
 */
public abstract class AbstractFormView<T extends BaseDomainObject> extends StackPane implements IStackedScreenProperties,FormPageView {

    private final Logger logger = LoggerFactory.getLogger(AbstractFormView.class);
    public static final String FORM_PAGES = "form-pages";

    protected DataPageController<T> controller;
    private ValidationSupport validationSupport;
    private StackedScreen stackedScreen;
    private List<IGenericComponent> formComponents = new ArrayList<IGenericComponent>();
    private List<ActionCommand> commandButtons = new ArrayList<ActionCommand>();

    public final int BUTTON_SPACING = 25;
    // commandButtons
    protected Label lblformTitle;

    /**
     * Creates form view
     *
     * @param controller form's controller
     */
    public AbstractFormView(DataPageController<T> controller) {
        super();
        this.controller = controller;
        this.stackedScreen = new StackedScreen();
        init();
    }

    public AbstractFormView(DataPageController<T> controller,StackedScreen stackedScreen) {
        super();
        this.controller = controller;
        this.stackedScreen = stackedScreen;
        init();
    }

    public void init(){
        buildView();
    }

    public DataPageController<T> getController() {
        return this.controller;
    }

    private void buildView() {
        logger.debug("build form view");
        BorderPane borderPane = new BorderPane();
        borderPane.getStyleClass().add("form-view");
        Node centerNode = buildFormView();
        borderPane.setTop(getHeaderBar());
        borderPane.setCenter(centerNode);
        borderPane.getCenter().getStyleClass().add("form-center");
        borderPane.setBottom(buildButtonPane());
        getChildren().add(borderPane);
        stackedScreen.addScreen(this, (IStackedScreenProperties) this);
    }

    private HBox getHeaderBar() {
        logger.debug("get header bar");
        HBox headerBar = new HBox();
        headerBar.getStyleClass().add("form-header");

        lblformTitle = ViewHelpers.createFontIconLabel(getFontIcon(), "");
        lblformTitle.getStyleClass().add("form-title");

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        headerBar.getChildren().addAll(lblformTitle, spacer);

        return headerBar;
    }

    private HBox buildButtonPane() {
        commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.save"),AppKeyCode.getKeyCode("SCD-S"), event -> onSave()));
        commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.saveAndClose"),AppKeyCode.getKeyCode("SCD-A"), event -> onSaveAndClose()));
        commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.delete"),AppKeyCode.getKeyCode("SCD-D"), event -> onDelete()));
        commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.clear"),AppKeyCode.getKeyCode("SCD-C"), event -> onClear()));
        if (hasPrintActions()) {
            commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.printPreview"),AppKeyCode.getKeyCode("SCD-R"), event -> {
                if (checkFormForReport()) {
                    /*getController().onFormPrintPreview(getCurrentEntity().getId());*/
                }
            }));

            commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.print"),AppKeyCode.getKeyCode("SCD-P"), event -> {
                if (checkFormForReport()) {
                   /* getController().onFormPrint(getCurrentEntity().getId());*/
                }
            }));

            commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.pdf"),AppKeyCode.getKeyCode("SCD-F"), event -> {
                if (checkFormForReport()) {
                    /*getController().onFormPdf(getCurrentEntity().getId());*/
                }
            }));
        }

        commandButtons.add(new ActionCommand(I18n.COMMON.getString("action.close"),new KeyCodeCombination(KeyCode.ESCAPE, KeyCombination.SHIFT_DOWN), event -> onCloseForm()));

        List<ActionCommand> extraCommands = new ArrayList<ActionCommand>();
        addMoreActionCommands(extraCommands);
        commandButtons.addAll(extraCommands);

        HBox buttonBar = new HBox();
        buttonBar.getStyleClass().add("form-button-bar");
        buttonBar.setAlignment(Pos.CENTER);
        buttonBar.setSpacing(BUTTON_SPACING);
        for(ActionCommand command : commandButtons){
            buttonBar.getChildren().add(command);
        }
        return buttonBar;
    }

    /**
     * Register the generic component.
     *
     * @param components
     */
    public void registerComponents(IGenericComponent... components){
        for(IGenericComponent component: components) {
            this.formComponents.add(component);
        }
    }

    /**
     * Add more commandButtons if required by the derived classes
     *
     * @param commands
     */
    public abstract void addMoreActionCommands(List<ActionCommand> commands);

    /**
     * Builds form view control
     *
     * @return center node of the form view
     */
    public abstract Node buildFormView();

    /**
     * If the form view has print actions, this method should be overridden.
     * Form view has no default print actions.
     *
     * @return true if the form view has print actions, otherwise false.
     */
    public Boolean hasPrintActions() {
        return false;
    }

    /**
     * Gets the current entity object
     *
     * @return entity of the form view
     */
    public abstract T getCurrentEntity();

    /**
     * Pop data model values to form view
     */
    public abstract void pop();

    /**
     * Retrieve latest entity from the datasource
     */
    public abstract void loadLatestEntity();

    /**
     * Push form view values to data model
     */
    public abstract void push();

    /**
     * If the fields of the form view is valid, save the data of the form view.
     */
    private void onSave() {
        logger.debug("form view save action");
        if (isFormValid()) {
            push();
            controller.onSave(getCurrentEntity());
            refreshForm();
        }
    }

    /**
     * If the fields of the form view are valid, save the data of the form view and close the form view.
     */
    private void onSaveAndClose() {
        logger.debug("form view save and close action");
        if (isFormValid()) {
            push();
            controller.onSave(getCurrentEntity());
            onCloseForm();
        }
    }

    /**
     * If the fields of the form view are valid, save the data of the form view and close the form view.
     */
    protected void onDelete() {
         logger.debug("form view on delete action");
    }

    /**
     * Clears all the data from the formComponents
     *
     */
    private void onClear(){
        for(IGenericComponent component: formComponents){
            if(!component.isReadonly()) {
                component.clearDataValue();
            }
        }
    }



    private boolean isFormValid() {
        logger.debug("form view validation");
        if (getValidationSupport().isInvalid()) {
            getValidationSupport().initInitialDecoration();
            /*if (DesktopFxApplication.get().getPreferences().isShowInfoPopups()) {
                Notifications.create()
                        .text(I18n.COMMON.getString("notification.saveValidationError"))
                        .position(Pos.TOP_RIGHT).showWarning();
            }*/
        }
        return !getValidationSupport().isInvalid();
    }

    /**
     * Gets validation support of the form view.
     *
     * @return validation support
     */
    protected ValidationSupport getValidationSupport() {
        if (validationSupport == null) {
            validationSupport = new ValidationSupport();
            validationSupport.setValidationDecorator(new GraphicValidationDecoration());
        }
        return validationSupport;
    }

    public void showDialog() {
        loadLatestEntity();
        pop();
        //showAndWait();
    }

    private boolean checkFormForReport() {
        /*if (getCurrentEntity().getId() == null) {
            MessageBox.create()
                    .owner(DesktopFxApplication.get().getMainStage())
                    .contentText(I18n.COMMON.getString("reports.formNotSaved"))
                    .showWarning();
            return false;
        }*/
        return true;
    }

    private void onCloseForm() {
        disposeScreen();
    }

    @Override
    public IAccelerator getAccelerator() {
        return new IAccelerator() {
            @Override
            public Map<String, Map<KeyCodeCombination, Runnable>> getAccelerators() {
                Map<String,Map<KeyCodeCombination, Runnable>> combinations = new HashMap<String,Map<KeyCodeCombination, Runnable>>();
                Map<KeyCodeCombination,Runnable> childAccelerators = new HashMap<KeyCodeCombination,Runnable>();
                Map<KeyCodeCombination,Runnable> parentAccelerators = new HashMap<KeyCodeCombination,Runnable>();
                for(ActionCommand command: commandButtons){
                    if(!command.isChildAccelarator()) {
                        parentAccelerators.put(command.getKeyCodeCombination(), command::fire);
                    }else{
                        childAccelerators.put(command.getKeyCodeCombination(), command::fire);
                    }
                }
                combinations.put(IAccelerator.CHILD_ACCELERATORS,childAccelerators);
                combinations.put(IAccelerator.PARENT_ACCELERATORS,parentAccelerators);
                return combinations;
            }
        };
    }

    @Override
    public void disposeScreen() {
        stackedScreen.removeScreen();
    }

    @Override
    public String getScreenTitle() {
        return getTitle();
    }

    public abstract void refreshForm();

    public AbstractFormView() {
        super();
    }

    public Label getLblformTitle() {
        return lblformTitle;
    }

    public void setLblformTitle(Label lblformTitle) {
        this.lblformTitle = lblformTitle;
    }
}
