package com.desktopapp.fx.common;

import javafx.collections.ObservableMap;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

import java.util.Map;
import java.util.Set;

/**
 * Created by diwakar009 on 24/6/2019.
 */
public interface IAccelerator {

    final Runnable BLANK_RUNNABLE = new Runnable() {
        @Override
        public void run() {}
    };

    public static final String PARENT_ACCELERATORS = "P";
    public static final String CHILD_ACCELERATORS = "C";

    public Map<String, Map<KeyCodeCombination,Runnable>> getAccelerators();

    public static void removeAccelerator(ObservableMap<KeyCombination, Runnable> accelerators, Map<KeyCodeCombination, Runnable> keyCodeCombination){
        keyCodeCombination.forEach((combination, runnable) -> {
            accelerators.replace(combination, BLANK_RUNNABLE);
        });
    }

    public static void addAccelerator(ObservableMap<KeyCombination, Runnable> accelerators, Map<KeyCodeCombination, Runnable> keyCodeCombination){
        keyCodeCombination.forEach((combination, runnable) -> {
            accelerators.put(combination, runnable);
        });
    }

    public static void addAllAccelerator(ObservableMap<KeyCombination, Runnable> accelerators, Map<String, Map<KeyCodeCombination, Runnable>> keyCodeCombinations){
        keyCodeCombinations.forEach((accelerator, keyCodeCombination) -> {
            keyCodeCombination.forEach((combination, runnable) -> {
                accelerators.put(combination, runnable);
            });
        });
    }

    public static void removeAllAccelerator(ObservableMap<KeyCombination, Runnable> accelerators, Map<String, Map<KeyCodeCombination, Runnable>> keyCodeCombinations){
        keyCodeCombinations.forEach((accelerator, keyCodeCombination) -> {
            keyCodeCombination.forEach((combination, runnable) -> {
                accelerators.replace(combination, BLANK_RUNNABLE);
            });
        });
    }

    public static void removeChildAccelerators(ObservableMap<KeyCombination, Runnable> accelerators, Map<String, Map<KeyCodeCombination, Runnable>> keyCodeCombinations){
        keyCodeCombinations.forEach((accelerator, keyCodeCombination) -> {
            if(CHILD_ACCELERATORS.equals(accelerator)) {
                keyCodeCombination.forEach((combination, runnable) -> {
                    accelerators.replace(combination, BLANK_RUNNABLE);
                });
            }
        });
    }

    public static void addChildAccelerators(ObservableMap<KeyCombination, Runnable> accelerators, Map<String, Map<KeyCodeCombination, Runnable>> keyCodeCombinations){
        keyCodeCombinations.forEach((accelerator, keyCodeCombination) -> {
            if(CHILD_ACCELERATORS.equals(accelerator)) {
                keyCodeCombination.forEach((combination, runnable) -> {
                    accelerators.put(combination, runnable);
                });
            }
        });
    }

}
