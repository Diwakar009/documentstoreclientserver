package com.desktopapp.fx.common;

/**
 * Created by diwakar009 on 21/8/2019.
 */
public interface IStackedScreenProperties {

    public static final String BLANK = "";

    /**
     * Get Accelerator of the screen
     * @return
     */
    IAccelerator getAccelerator();

    /**
     * Dispose screen
     *
     */
    void disposeScreen();


    /**
     * get the title
     */
    default String getScreenTitle(){
        return BLANK;
    }

}
