package com.desktopapp.fx.common;

/**
 * functional interface for task completion
 */
public interface InitCompletionHandler {

		void complete();
}