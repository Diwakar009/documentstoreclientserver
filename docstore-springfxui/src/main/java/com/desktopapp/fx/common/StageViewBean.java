package com.desktopapp.fx.common;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Created by diwakar009 on 13/12/2018.
 */
public class StageViewBean {

    private Stage stage;
    private Scene scene;

    public StageViewBean(Stage stage, Scene scene) {
        this.stage = stage;
        this.scene = scene;
    }



    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
