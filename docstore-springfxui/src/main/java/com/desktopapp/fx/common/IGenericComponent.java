package com.desktopapp.fx.common;

/**
 * Generic component methods
 *  
 * @author diwakarchoudhury
 */
public interface IGenericComponent {
	public void setAsReadOnly(boolean readOnly);

	public void destroyComponent();
	
	/**
	 * If the component data is applicable for the data exchange with server.
	 * 
	 * @return
	 */
	default public boolean isDataExchangeApplicable(){
		return false;
	};
	
	default public void setDataValue(String value){
		
	};
	
	default public String getDataValue(){
		return "";
	};
	
	default public boolean isComponentShowing(){
		return false;
	};

	default public boolean validateInput(){
		return true;
	};
	default public void clearDataValue(){};
	default public boolean isReadonly(){return false;};

}
