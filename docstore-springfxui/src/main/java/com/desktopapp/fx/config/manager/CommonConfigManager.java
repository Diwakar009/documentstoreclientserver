package com.desktopapp.fx.config.manager;

import com.desktopapp.fx.config.OverrideProperties;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

/**
 * Created by diwakar009 on 16/12/2018.
 */
public abstract class CommonConfigManager<T> implements OverrideProperties {

    private Class<T> clazz;

    public CommonConfigManager(Class<T> clazz) {
        this.clazz = clazz;
    }

    protected T readConfig(){
        InputStream inputStream = CommonConfigManager.class.getClassLoader().getResourceAsStream(getConfigJSONResource());
        try {
            if (inputStream != null) {
                StringWriter splashJSON = new StringWriter();
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                Reader reader = new InputStreamReader(inputStream, "UTF-8");
                T config  = new Gson().fromJson(reader, clazz);
                return config;
            }
        }catch (Exception e){
            //TODO Exception handling
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public void overrideProperties() {

    }

    public abstract String getConfigJSONResource();
}
