package com.desktopapp.fx.config.manager;

import com.desktopapp.fx.app.AppFontIcons;
import com.desktopapp.fx.config.SplashConfig;
import com.desktopapp.fx.control.Splash;
import com.desktopapp.fx.control.fonticon.FontIconColor;
import com.desktopapp.fx.control.fonticon.FontIconFactory;
import com.desktopapp.fx.control.fonticon.FontIconSize;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.commons.lang.StringUtils;

/**
 * Created by diwakar009 on 16/12/2018.
 */
public class SplashConfigManager extends CommonConfigManager<SplashConfig> {

    private Splash splash;

    public SplashConfigManager(Splash splash) {
        super(SplashConfig.class);
        this.splash=splash;
    }

    @Override
    public void overrideProperties() {
        super.overrideProperties();

        SplashConfig splashConfig  = readConfig();
        if(splashConfig != null){
            if(splashConfig.getSplashImagePath() == null || StringUtils.isBlank(splashConfig.getSplashImagePath())) {
                AppFontIcons splashIcon = AppFontIcons.valueOf(splashConfig.getSplashIcon());
                FontIconSize fontIconSize = FontIconSize.valueOf(splashConfig.getFontIconSize());
                FontIconColor splashIconColor = FontIconColor.valueOf(splashConfig.getSplashIconColor());

                this.splash.setSplashImageFontIcon(FontIconFactory.createIcon(splashIcon, (fontIconSize!=null?fontIconSize: FontIconSize.XXXL), splashIconColor));
                this.splash.setSplashImageView(null);

            }else{
                String splashImagePath=splashConfig.getSplashImagePath();
                this.splash.setSplashImageView(new ImageView(new Image(splashImagePath)));
                this.splash.setSplashImageFontIcon(null);

            }
        }
   }

    @Override
    public String getConfigJSONResource() {
        return "productconfig/splash.json";
    }
}
