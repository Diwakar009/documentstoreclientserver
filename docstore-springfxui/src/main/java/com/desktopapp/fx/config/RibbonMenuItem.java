package com.desktopapp.fx.config;

import javafx.scene.Node;

import java.util.List;

/**
 * Created by diwakar009 on 11/3/2019.
 */
public class RibbonMenuItem {
    private String menuDisplayText;
    private List<Node> ribbonItems;

    public RibbonMenuItem(String menuDisplayText, List<Node> ribbonItems) {
        this.menuDisplayText = menuDisplayText;
        this.ribbonItems=ribbonItems;
    }

    public String getMenuDisplayText() {
        return menuDisplayText;
    }

    public void setMenuDisplayText(String menuDisplayText) {
        this.menuDisplayText = menuDisplayText;
    }

    public List<Node> getRibbonItems() {
        return ribbonItems;
    }

    public void setRibbonItems(List<Node> ribbonItems) {
        this.ribbonItems = ribbonItems;
    }
}
