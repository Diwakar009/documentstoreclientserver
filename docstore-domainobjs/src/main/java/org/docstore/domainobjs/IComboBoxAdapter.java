package org.docstore.domainobjs;

/**
 * Created by diwakar009 on 6/12/2018.
 */
public interface IComboBoxAdapter<T> {
    public String getSelectedItemText(T t);
}
