package org.docstore.domainobjs.jsonwrapper;


public interface IJSONObject {
	
	public void copyEntityToJSON();
	
	public void copyJSONToEntity();

}
