package org.docstore.domainobjs.jsonwrapper;

import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.Map;

public class UISettingsJSON {
    
    	@Expose
    	private Map<String,String> uiSettingMap = new HashMap<String, String>();

	public Map<String,String> getUiSettingMap() {
	    return uiSettingMap;
	}

	public void setUiSettingMap(Map<String, String> uiSettingMap) {
	    this.uiSettingMap = uiSettingMap;
	}

	
	
}
