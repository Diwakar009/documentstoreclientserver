package org.docstore.domainobjs.json;

import com.docstore.framework.IEntity;
import com.docstore.model.BaseEntity;
import com.docstore.model.DocContent;
import com.docstore.model.DocContentBlob;
import com.docstore.model.DocHolder;
import com.docstore.util.DateDeserializer;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the doc_content database table.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DocContentDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = DocContentDO.class.getName();

	@JsonProperty("id")
	private Long id;

	@JsonProperty("docContentKeywords")
	private String docContentKeywords;

	@JsonProperty("docContentName")
	private String docContentName;

	@JsonProperty("docFileExtension")
	private String docFileExtension;

	@JsonProperty("docFileName")
	private String docFileName;

	@JsonProperty("docDirPath")
	private String docDirPath;

	@JsonProperty("docLastModifiedTime")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	//@JsonDeserialize(using = DateDeserializer.class)
	private Date docLastModifiedTime;

	@JsonProperty("docHolderId")
	private Long docHolderId;

	@JsonIgnore
	private byte[] fileContent;

	@JsonIgnore
	private Long docContentBlobId;



	public DocContentDO() {
	}

	public DocContentDO(DocContent content) {
		copyEntity2DomainObject(content);
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDocContentKeywords() {
		return this.docContentKeywords;
	}

	public void setDocContentKeywords(String docContentKeywords) {
		this.docContentKeywords = docContentKeywords;
	}

	public String getDocContentName() {
		return this.docContentName;
	}

	public void setDocContentName(String docContentName) {
		this.docContentName = docContentName;
	}
	
	public String getDocFileExtension() {
		return docFileExtension;
	}

	public void setDocFileExtension(String docFileExtension) {
		this.docFileExtension = docFileExtension;
	}

	public String getDocFileName() {
		return docFileName;
	}

	public void setDocFileName(String docFileName) {
		this.docFileName = docFileName;
	}

	public String getDocDirPath() {
		return docDirPath;
	}

	public void setDocDirPath(String docFilePath) {
		this.docDirPath = docFilePath;
	}

	public Date getDocLastModifiedTime() {
	    return docLastModifiedTime;
	}

	public void setDocLastModifiedTime(Date docLastModifiedTime) {
	    this.docLastModifiedTime = docLastModifiedTime;
	}

	public Long getDocHolderId() {
		return docHolderId;
	}

	public void setDocHolderId(Long docHolderId) {
		this.docHolderId = docHolderId;
	}

	@JsonIgnore
	public byte[] getFileContent() {
		return fileContent;
	}

	@JsonIgnore
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	@JsonIgnore
	public Long getDocContentBlobId() {
		return docContentBlobId;
	}

	@JsonIgnore
	public void setDocContentBlobId(Long docContentBlobId) {
		this.docContentBlobId = docContentBlobId;
	}

	@Override
	@JsonIgnore
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public DocContent copyDomainObject2Entity(BaseEntity entity) {

		if(entity == null){
			entity = new DocContent();
		}

		DocContent docContent = (DocContent)entity;
		docContent.setCreatedBy(this.getCreatedBy());
		docContent.setUpdatedBy(this.getUpdatedBy());
		docContent.setCreatedAt(this.getCreatedAt());
		docContent.setUpdatedAt(this.getUpdatedAt());

		docContent.setId(this.getId());
		docContent.setDocFileName(this.getDocFileName());
		docContent.setDocFileExtension(this.getDocFileExtension());
		docContent.setDocContentKeywords(this.getDocContentKeywords());
		docContent.setDocContentName(this.getDocContentName());
		docContent.setDocDirPath(this.getDocDirPath());
		docContent.setDocLastModifiedTime(this.getDocLastModifiedTime());

		if(this.getDocHolderId() != null) {
			docContent.setDocHolder(new DocHolder());
			docContent.getDocHolder().setId(this.getDocHolderId());
		}

		return docContent;
	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {

		DocContent docContent = (DocContent)entity;
		setCreatedBy(docContent.getCreatedBy());
		setUpdatedBy(docContent.getUpdatedBy());
		setCreatedAt(docContent.getCreatedAt());
		setUpdatedAt(docContent.getUpdatedAt());

		setId(docContent.getId());
		setDocFileName(docContent.getDocFileName());
		setDocFileExtension(docContent.getDocFileExtension());
		setDocContentKeywords(docContent.getDocContentKeywords());
		setDocContentName(docContent.getDocContentName());
		setDocDirPath(docContent.getDocDirPath());
		setDocLastModifiedTime(docContent.getDocLastModifiedTime());
		setDocHolderId(docContent.getDocHolder().getId());
	}
}