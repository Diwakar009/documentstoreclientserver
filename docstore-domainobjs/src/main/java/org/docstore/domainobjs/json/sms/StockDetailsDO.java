/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package org.docstore.domainobjs.json.sms;

import com.docstore.framework.IEntity;
import com.docstore.model.BaseEntity;
import com.docstore.model.sms.PartyDetails;
import com.docstore.model.sms.Stock;
import com.docstore.model.sms.StockDetails;
import com.fasterxml.jackson.annotation.*;
import org.docstore.domainobjs.json.BaseDomainObject;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * StockDetailsDO .
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class StockDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;
	public static String CLASS_NAME = StockDetailsDO.class.getName();


	public StockDetailsDO() {
		init();
	}

	public StockDetailsDO(StockDetails stock) {
		init();
		copyEntity2DomainObject(stock);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("stockCode")
	private String stockCode;

	@JsonProperty("stockVarietyCode")
	private String stockVarietyCode;

	@JsonProperty("transactionType")
	private String transactionType;

	@JsonProperty("transactionDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date transactionDate;

	@JsonProperty("noOfPackets")
	private Long noOfPackets;

	@JsonProperty("totalWeightment")
	private Double totalWeightment;

	@JsonProperty("plusOrMinusOnTotal")
	private Character plusOrMinusOnTotal;


	/*@JsonProperty("packetOpeningBalance")
	private Long packetOpeningBalance;

	@JsonProperty("weighmentOpeningBalance")
	private Double weighmentOpeningBalance;

	@JsonProperty("packetClosingBalance")
	private Long packetClosingBalance;

	@JsonProperty("weightmentClosingBalance")
	private Double weightmentClosingBalance;*/

	@JsonProperty("remarks")
	private String remarks;

	@JsonProperty("onBeHalf")
	private String onBeHalf;

	@JsonProperty("moreDetails")
	private String moreDetails;

	@JsonProperty("partyDetailsDO")
	private PartyDetailsDO partyDetailsDO;

	@JsonProperty("transportDetailsDO")
	private TransportDetailsDO transportDetailsDO;

	@JsonProperty("deliveryDetailsDO")
	private DeliveryDetailsDO deliveryDetailsDO;

	@JsonProperty("stockDO")
	private StockDO stockDO;


	public void init(){
		/*partyDetailsDO = new PartyDetailsDO();
		transportDetailsDO = new TransportDetailsDO();
		deliveryDetailsDO = new DeliveryDetailsDO();
		stockDO = new StockDO();*/
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getNoOfPackets() {
		return noOfPackets;
	}

	public void setNoOfPackets(Long noOfPackets) {
		this.noOfPackets = noOfPackets;
	}

	public Double getTotalWeightment() {
		return totalWeightment;
	}

	public void setTotalWeightment(Double totalWeightment) {
		this.totalWeightment = totalWeightment;
	}

	public Character getPlusOrMinusOnTotal() {
		return plusOrMinusOnTotal;
	}

	public void setPlusOrMinusOnTotal(Character plusOrMinusOnTotal) {
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
	}

	/*	public Long getPacketOpeningBalance() {
		return packetOpeningBalance;
	}

	public void setPacketOpeningBalance(Long packetOpeningBalance) {
		this.packetOpeningBalance = packetOpeningBalance;
	}

	public Double getWeighmentOpeningBalance() {
		return weighmentOpeningBalance;
	}

	public void setWeighmentOpeningBalance(Double weighmentOpeningBalance) {
		this.weighmentOpeningBalance = weighmentOpeningBalance;
	}

	public Long getPacketClosingBalance() {
		return packetClosingBalance;
	}

	public void setPacketClosingBalance(Long packetClosingBalance) {
		this.packetClosingBalance = packetClosingBalance;
	}

	public Double getWeightmentClosingBalance() {
		return weightmentClosingBalance;
	}

	public void setWeightmentClosingBalance(Double weightmentClosingBalance) {
		this.weightmentClosingBalance = weightmentClosingBalance;
	}*/

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOnBeHalf() {
		return onBeHalf;
	}

	public void setOnBeHalf(String onBeHalf) {
		this.onBeHalf = onBeHalf;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public PartyDetailsDO getPartyDetailsDO() {
		return partyDetailsDO;
	}

	public void setPartyDetailsDO(PartyDetailsDO partyDetailsDO) {
		this.partyDetailsDO = partyDetailsDO;
	}

	public TransportDetailsDO getTransportDetailsDO() {
		return transportDetailsDO;
	}

	public void setTransportDetailsDO(TransportDetailsDO transportDetailsDO) {
		this.transportDetailsDO = transportDetailsDO;
	}

	public DeliveryDetailsDO getDeliveryDetailsDO() {
		return deliveryDetailsDO;
	}

	public void setDeliveryDetailsDO(DeliveryDetailsDO deliveryDetailsDO) {
		this.deliveryDetailsDO = deliveryDetailsDO;
	}

	public StockDO getStockDO() {
		return stockDO;
	}

	public void setStockDO(StockDO stockDO) {
		this.stockDO = stockDO;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public StockDetails copyDomainObject2Entity(BaseEntity entity) {

		StockDetails stockDetails = null;

		if(entity == null){
			stockDetails = new StockDetails();
		}else {
			stockDetails = (StockDetails)entity;
		}

		stockDetails.setCreatedBy(this.getCreatedBy());
		stockDetails.setUpdatedBy(this.getUpdatedBy());
		stockDetails.setCreatedAt(this.getCreatedAt());
		stockDetails.setUpdatedAt(this.getUpdatedAt());

		stockDetails.setId(this.getId());
		stockDetails.setStockCode(this.getStockCode());
		stockDetails.setStockVarietyCode(this.getStockVarietyCode());
		stockDetails.setTransactionType(this.getTransactionType());
		stockDetails.setIndustryCode(this.getIndustryCode());
		stockDetails.setNoOfPackets(this.getNoOfPackets());
		stockDetails.setTotalWeightment(this.getTotalWeightment());
		stockDetails.setPlusOrMinusOnTotal(this.getPlusOrMinusOnTotal());
		/*stockDetails.setPacketClosingBalance(this.getPacketClosingBalance());
		stockDetails.setPacketOpeningBalance(this.getPacketOpeningBalance());
		stockDetails.setWeighmentOpeningBalance(this.getWeighmentOpeningBalance());
		stockDetails.setWeightmentClosingBalance(this.getWeightmentClosingBalance());*/
		stockDetails.setRemarks(this.getRemarks());
		stockDetails.setOnBeHalf(this.getOnBeHalf());
		stockDetails.setMoreDetails(this.getMoreDetails());
		stockDetails.setTransactionDate(this.getTransactionDate());

		if(this.getPartyDetailsDO() != null) {
			stockDetails.setPartyDetails(this.getPartyDetailsDO().copyDomainObject2Entity(stockDetails.getPartyDetails()));
		}

		if(this.getDeliveryDetailsDO() != null) {
			stockDetails.setDeliveryDetails(this.getDeliveryDetailsDO().copyDomainObject2Entity(stockDetails.getDeliveryDetails()));
		}

		if(this.getTransportDetailsDO() != null) {
			stockDetails.setTransportDetails(this.getTransportDetailsDO().copyDomainObject2Entity(stockDetails.getTransportDetails()));
		}

		if(this.getStockDO() != null) {
			stockDetails.setStock(this.getStockDO().copyDomainObject2Entity(stockDetails.getStock()));
		}

		return stockDetails;
	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		StockDetails stockDetails = (StockDetails)entity;
		this.setCreatedBy(stockDetails.getCreatedBy());
		this.setUpdatedBy(stockDetails.getUpdatedBy());
		this.setCreatedAt(stockDetails.getCreatedAt());
		this.setUpdatedAt(stockDetails.getUpdatedAt());

		this.setCreatedBy(stockDetails.getCreatedBy());
		this.setUpdatedBy(stockDetails.getUpdatedBy());
		this.setCreatedAt(stockDetails.getCreatedAt());
		this.setUpdatedAt(stockDetails.getUpdatedAt());

		this.setId(stockDetails.getId());
		this.setStockCode(stockDetails.getStockCode());
		this.setStockVarietyCode(stockDetails.getStockVarietyCode());
		this.setTransactionType(stockDetails.getTransactionType());
		this.setIndustryCode(stockDetails.getIndustryCode());
		this.setNoOfPackets(stockDetails.getNoOfPackets());
		this.setTotalWeightment(stockDetails.getTotalWeightment());
		this.setPlusOrMinusOnTotal(stockDetails.getPlusOrMinusOnTotal());
		/*this.setPacketClosingBalance(stockDetails.getPacketClosingBalance());
		this.setPacketOpeningBalance(stockDetails.getPacketOpeningBalance());
		this.setWeighmentOpeningBalance(stockDetails.getWeighmentOpeningBalance());
		this.setWeightmentClosingBalance(stockDetails.getWeightmentClosingBalance());*/
		this.setRemarks(stockDetails.getRemarks());
		this.setOnBeHalf(stockDetails.getOnBeHalf());
		this.setMoreDetails(stockDetails.getMoreDetails());
		this.setTransactionDate(stockDetails.getTransactionDate());

		if(stockDetails.getPartyDetails() != null) {
			this.setPartyDetailsDO(new PartyDetailsDO());
			this.getPartyDetailsDO().copyEntity2DomainObject(stockDetails.getPartyDetails());
		}

		if(stockDetails.getDeliveryDetails() != null){
			this.setDeliveryDetailsDO(new DeliveryDetailsDO());
			this.getDeliveryDetailsDO().copyEntity2DomainObject(stockDetails.getDeliveryDetails());
		}

		if(stockDetails.getTransportDetails() != null){
			this.setTransportDetailsDO(new TransportDetailsDO());
			this.getTransportDetailsDO().copyEntity2DomainObject(stockDetails.getTransportDetails());
		}

		if(stockDetails.getStock() != null){
			this.setStockDO(new StockDO());
			this.getStockDO().copyEntity2DomainObject(stockDetails.getStock());
		}
	}

}