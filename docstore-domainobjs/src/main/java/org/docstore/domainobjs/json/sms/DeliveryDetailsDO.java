/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package org.docstore.domainobjs.json.sms;

import com.docstore.model.BaseEntity;
import com.docstore.model.sms.Contract;
import com.docstore.model.sms.DeliveryDetails;
import com.docstore.model.sms.PartyDetails;
import com.docstore.model.sms.TransportDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.docstore.domainobjs.json.BaseDomainObject;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * DeliveryDetailsDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class DeliveryDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = ContractDO.class.getName();

	public DeliveryDetailsDO() {
	}

	public DeliveryDetailsDO(DeliveryDetails deliveryDetails) {
		copyEntity2DomainObject(deliveryDetails);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("stockCode")
	private String stockCode;

	@JsonProperty("stockVarietyCode")
	private String stockVarietyCode;

	@JsonProperty("deliveryTypeCode")
	private String deliveryTypeCode;

	@JsonProperty("deliveryDepo")
	private String deliveryDepo;

	@JsonProperty("deliveryLatNo")
	private String deliveryLatNo;

	@JsonProperty("deliveryAddress")
	private String deliveryAddress;

	@JsonProperty("moreDetails")
	private String moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}


	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getDeliveryTypeCode() {
		return deliveryTypeCode;
	}

	public void setDeliveryTypeCode(String deliveryTypeCode) {
		this.deliveryTypeCode = deliveryTypeCode;
	}

	public String getDeliveryDepo() {
		return deliveryDepo;
	}

	public void setDeliveryDepo(String deliveryDepo) {
		this.deliveryDepo = deliveryDepo;
	}

	public String getDeliveryLatNo() {
		return deliveryLatNo;
	}

	public void setDeliveryLatNo(String deliveryLatNo) {
		this.deliveryLatNo = deliveryLatNo;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public DeliveryDetails copyDomainObject2Entity(BaseEntity entity) {

		DeliveryDetails deliveryDetails = null;

		if(entity == null){
			deliveryDetails = new DeliveryDetails();
		}else {
			deliveryDetails = (DeliveryDetails)entity;
		}

		deliveryDetails.setCreatedBy(this.getCreatedBy());
		deliveryDetails.setUpdatedBy(this.getUpdatedBy());
		deliveryDetails.setCreatedAt(this.getCreatedAt());
		deliveryDetails.setUpdatedAt(this.getUpdatedAt());

		deliveryDetails.setId(this.getId());
		deliveryDetails.setIndustryCode(this.getIndustryCode());
		deliveryDetails.setStockCode(this.getStockCode());
		deliveryDetails.setStockVarietyCode(this.getStockVarietyCode());
		deliveryDetails.setDeliveryTypeCode(this.getDeliveryTypeCode());
		deliveryDetails.setDeliveryDepo(this.getDeliveryDepo());
		deliveryDetails.setDeliveryAddress(this.getDeliveryAddress());
		deliveryDetails.setDeliveryLatNo(this.getDeliveryLatNo());
		deliveryDetails.setMoreDetails(this.getMoreDetails());
		return deliveryDetails;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		DeliveryDetails deliveryDetails = (DeliveryDetails)entity;
		this.setCreatedBy(deliveryDetails.getCreatedBy());
		this.setUpdatedBy(deliveryDetails.getUpdatedBy());
		this.setCreatedAt(deliveryDetails.getCreatedAt());
		this.setUpdatedAt(deliveryDetails.getUpdatedAt());

		this.setId(deliveryDetails.getId());
		this.setStockCode(deliveryDetails.getStockCode());
		this.setStockVarietyCode(deliveryDetails.getStockVarietyCode());
		this.setIndustryCode(deliveryDetails.getIndustryCode());
		this.setDeliveryTypeCode(deliveryDetails.getDeliveryTypeCode());
		this.setDeliveryDepo(deliveryDetails.getDeliveryDepo());
		this.setDeliveryAddress(deliveryDetails.getDeliveryAddress());
		this.setDeliveryLatNo(deliveryDetails.getDeliveryLatNo());
		this.setMoreDetails(deliveryDetails.getMoreDetails());
	}

	@Override
	public String toString() {
		return "DeliveryDetailsDO{" +
				"id=" + id +
				", industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				", deliveryTypeCode='" + deliveryTypeCode + '\'' +
				", deliveryDepo='" + deliveryDepo + '\'' +
				", deliveryLatNo='" + deliveryLatNo + '\'' +
				", deliveryAddress='" + deliveryAddress + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}