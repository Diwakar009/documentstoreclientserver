package org.docstore.domainobjs.json;

import java.io.Serializable;

import com.docstore.framework.IEntity;
import com.docstore.model.DocContentBlob;
import org.docstore.domainobjs.IDomainObject;


/**
 * The persistent class for the doc_content_blob database table.
 * 
 */
public class DocContentBlobDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = DocContentBlobDO.class.getName();

	private Long id;
	
	private byte[] docContentBlob;

	public DocContentBlobDO() {
	}

	public Long getId() {
		return this.id;
	}

	public void setid(Long id) {
		this.id = id;
	}

	public byte[] getDocContentBlob() {
		return this.docContentBlob;
	}

	public void setDocContentBlob(byte[] docContent) {
		this.docContentBlob = docContent;
	}

}