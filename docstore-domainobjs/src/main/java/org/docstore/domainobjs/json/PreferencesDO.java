/*
 * Copyright (C) 2017 Cem Ikta
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 */

package org.docstore.domainobjs.json;

import com.docstore.model.AppUser;
import com.docstore.model.BaseEntity;
import com.docstore.model.Preferences;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Preference Domain Object
 *
 */
public class PreferencesDO extends BaseDomainObject implements Serializable {


    public static String CLASS_NAME = PreferencesDO.class.getName();

    private Long id;

    private Integer itemsPerPage;

    private Boolean showMessageDialog;

    private Boolean showInfoPopups;

    private Boolean showTipOfTheDay;

    private String reportExportDirectory;

    private String appTheme;

    public PreferencesDO() {
    }

    public PreferencesDO(Preferences preferences) {
        copyEntity2DomainObject(preferences);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Boolean isShowMessageDialog() {
        return showMessageDialog;
    }

    public void setShowMessageDialog(Boolean showMessageDialog) {
        this.showMessageDialog = showMessageDialog;
    }

    public Boolean isShowInfoPopups() {
        return showInfoPopups;
    }

    public void setShowInfoPopups(Boolean showInfoPopups) {
        this.showInfoPopups = showInfoPopups;
    }

    public Boolean isShowTipOfTheDay() {
        return showTipOfTheDay;
    }

    public void setShowTipOfTheDay(Boolean showTipOfTheDay) {
        this.showTipOfTheDay = showTipOfTheDay;
    }

    public String getReportExportDirectory() {
        return reportExportDirectory;
    }

    public void setReportExportDirectory(String reportExportDirectory) {
        this.reportExportDirectory = reportExportDirectory;
    }

    public String getAppTheme() {
        return appTheme;
    }

    public void setAppTheme(String appTheme) {
        this.appTheme = appTheme;
    }

    @Override
    public String toString() {
        return "PreferencesDO{" +
                "itemsPerPage=" + itemsPerPage +
                ", showMessageDialog=" + showMessageDialog +
                ", showInfoPopups=" + showInfoPopups +
                ", showTipOfTheDay=" + showTipOfTheDay +
                ", reportExportDirectory='" + reportExportDirectory + '\'' +
                ", appTheme=" + appTheme +
                '}';
    }

    @Override
    public Object getDomainObjectKey() {
        return id;
    }

    @Override
    public Preferences copyDomainObject2Entity(BaseEntity entity){
        Preferences preferences = (Preferences)entity;
        preferences.setCreatedBy(this.getCreatedBy());
        preferences.setUpdatedBy(this.getUpdatedBy());
        preferences.setCreatedAt(this.getCreatedAt());
        preferences.setUpdatedAt(this.getUpdatedAt());
        preferences.setId(this.getId());
        preferences.setItemsPerPage(this.getItemsPerPage());
        preferences.setShowMessageDialog(this.isShowMessageDialog());
        preferences.setShowInfoPopups(this.isShowInfoPopups());
        preferences.setShowTipOfTheDay(this.isShowTipOfTheDay());
        preferences.setReportExportDirectory(this.getReportExportDirectory());
        preferences.setAppTheme(this.getAppTheme());
        return preferences;
    }

    @Override
    public void copyEntity2DomainObject(BaseEntity entity){
        Preferences preferences = (Preferences) entity;
        this.setCreatedBy(preferences.getCreatedBy());
        this.setUpdatedBy(preferences.getUpdatedBy());
        this.setCreatedAt(preferences.getCreatedAt());
        this.setUpdatedAt(preferences.getUpdatedAt());
        this.setId(preferences.getId());
        this.setItemsPerPage(preferences.getItemsPerPage());
        this.setShowMessageDialog(preferences.isShowMessageDialog());
        this.setShowInfoPopups(preferences.isShowInfoPopups());
        this.setShowTipOfTheDay(preferences.isShowTipOfTheDay());
        this.setReportExportDirectory(preferences.getReportExportDirectory());
        this.setAppTheme(preferences.getAppTheme());
    }
}