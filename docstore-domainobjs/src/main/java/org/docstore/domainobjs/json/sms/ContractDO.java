/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package org.docstore.domainobjs.json.sms;

import com.docstore.model.BaseEntity;
import com.docstore.model.sms.Contract;
import com.docstore.model.sms.StockDetails;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.docstore.domainobjs.json.BaseDomainObject;

import java.io.Serializable;
import java.util.Date;

/**
 * ContractDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class ContractDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = ContractDO.class.getName();

	public ContractDO() {
	}

	public ContractDO(Contract contract) {
		copyEntity2DomainObject(contract);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("startDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date startDate;

	@JsonProperty("endDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date endDate;

	@JsonProperty("contractTypeCode")
	private String contractTypeCode;

	@JsonProperty("contractName")
	private String contractName;

	@JsonProperty("DeliveryProduct")
	private String DeliveryProduct;

	@JsonProperty("rawMaterial")
	private String rawMaterial;

	@JsonProperty("alotedWmtEntry")
	private Double alotedWmtEntry;

	@JsonProperty("alotedWmtAdjustment")
	private Double alotedWmtAdjustment;

	@JsonProperty("finalProductWmt")
	private Double finalProductWmt;

	@JsonProperty("lastLatNo")
	private Long lastLatNo;

	@JsonProperty("pendingProductWmt")
	private Double pendingProductWmt;

	@JsonProperty("recvRawMatrlWmt")
	private Double recvRawMatrlWmt;

	@JsonProperty("status")
	private String status;

	@JsonProperty("moreDetails")
	private String moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getContractTypeCode() {
		return contractTypeCode;
	}

	public void setContractTypeCode(String contractTypeCode) {
		this.contractTypeCode = contractTypeCode;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getDeliveryProduct() {
		return DeliveryProduct;
	}

	public void setDeliveryProduct(String deliveryProduct) {
		DeliveryProduct = deliveryProduct;
	}

	public String getRawMaterial() {
		return rawMaterial;
	}

	public void setRawMaterial(String rawMaterial) {
		this.rawMaterial = rawMaterial;
	}

	public Double getAlotedWmtEntry() {
		return alotedWmtEntry;
	}

	public void setAlotedWmtEntry(Double alotedWmtEntry) {
		this.alotedWmtEntry = alotedWmtEntry;
	}

	public Double getAlotedWmtAdjustment() {
		return alotedWmtAdjustment;
	}

	public void setAlotedWmtAdjustment(Double alotedWmtAdjustment) {
		this.alotedWmtAdjustment = alotedWmtAdjustment;
	}

	public Double getFinalProductWmt() {
		return finalProductWmt;
	}

	public void setFinalProductWmt(Double finalProductWmt) {
		this.finalProductWmt = finalProductWmt;
	}

	public Long getLastLatNo() {
		return lastLatNo;
	}

	public void setLastLatNo(Long lastLatNo) {
		this.lastLatNo = lastLatNo;
	}

	public Double getPendingProductWmt() {
		return pendingProductWmt;
	}

	public void setPendingProductWmt(Double pendingProductWmt) {
		this.pendingProductWmt = pendingProductWmt;
	}

	public Double getRecvRawMatrlWmt() {
		return recvRawMatrlWmt;
	}

	public void setRecvRawMatrlWmt(Double recvRawMatrlWmt) {
		this.recvRawMatrlWmt = recvRawMatrlWmt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}


	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public Contract copyDomainObject2Entity(BaseEntity entity) {
		Contract contract = (Contract)entity;
		contract.setCreatedBy(this.getCreatedBy());
		contract.setUpdatedBy(this.getUpdatedBy());
		contract.setCreatedAt(this.getCreatedAt());
		contract.setUpdatedAt(this.getUpdatedAt());

		contract.setId(this.getId());
		contract.setIndustryCode(this.getIndustryCode());
		contract.setContractTypeCode(this.getContractTypeCode());
		contract.setContractName(this.getContractName());
		contract.setStartDate(this.getStartDate());
		contract.setEndDate(this.getEndDate());
		contract.setDeliveryProduct(this.getDeliveryProduct());
		contract.setRawMaterial(this.getRawMaterial());
		contract.setRecvRawMatrlWmt(this.getRecvRawMatrlWmt());
		contract.setPendingProductWmt(this.getPendingProductWmt());
		contract.setLastLatNo(this.getLastLatNo());
		contract.setFinalProductWmt(this.getFinalProductWmt());
		contract.setAlotedWmtAdjustment(this.getAlotedWmtAdjustment());
		contract.setAlotedWmtEntry(this.getAlotedWmtEntry());
		contract.setStatus(this.getStatus());
		contract.setMoreDetails(this.getMoreDetails());
		return contract;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		Contract contract = (Contract) entity;
		this.setCreatedBy(contract.getCreatedBy());
		this.setUpdatedBy(contract.getUpdatedBy());
		this.setCreatedAt(contract.getCreatedAt());
		this.setUpdatedAt(contract.getUpdatedAt());

		this.setId(contract.getId());
		this.setIndustryCode(contract.getIndustryCode());
		this.setContractTypeCode(contract.getContractTypeCode());
		this.setContractName(contract.getContractName());
		this.setStartDate(contract.getStartDate());
		this.setEndDate(contract.getEndDate());
		this.setDeliveryProduct(contract.getDeliveryProduct());
		this.setRawMaterial(contract.getRawMaterial());
		this.setRecvRawMatrlWmt(contract.getRecvRawMatrlWmt());
		this.setPendingProductWmt(contract.getPendingProductWmt());
		this.setLastLatNo(contract.getLastLatNo());
		this.setFinalProductWmt(contract.getFinalProductWmt());
		this.setAlotedWmtAdjustment(contract.getAlotedWmtAdjustment());
		this.setAlotedWmtEntry(contract.getAlotedWmtEntry());
		this.setStatus(contract.getStatus());
		this.setMoreDetails(contract.getMoreDetails());
	}

}