/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package org.docstore.domainobjs.json.sms;

import com.docstore.model.BaseEntity;
import com.docstore.model.sms.DeliveryDetails;
import com.docstore.model.sms.StockDetails;
import com.docstore.model.sms.TransportDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.docstore.domainobjs.json.BaseDomainObject;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * TransportDetailsDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class TransportDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = TransportDetailsDO.class.getName();

	public TransportDetailsDO() {
	}

	public TransportDetailsDO(TransportDetails transportDetails) {
		copyEntity2DomainObject(transportDetails);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("stockCode")
	private String stockCode;

	@JsonProperty("stockVarietyCode")
	private String stockVarietyCode;

	@JsonProperty("transportTypeCode")
	private String transportTypeCode;

	@JsonProperty("rstNumber")
	private String rstNumber;

	@JsonProperty("vehicleNumber")
	private String vehicleNumber;

	@JsonProperty("vehicleCaptain")
	private String vehicleCaptain;

	@JsonProperty("moreDetails")
	private String moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public String getTransportTypeCode() {
		return transportTypeCode;
	}

	public void setTransportTypeCode(String transportTypeCode) {
		this.transportTypeCode = transportTypeCode;
	}

	public String getRstNumber() {
		return rstNumber;
	}

	public void setRstNumber(String rstNumber) {
		this.rstNumber = rstNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleCaptain() {
		return vehicleCaptain;
	}

	public void setVehicleCaptain(String vehicleCaptain) {
		this.vehicleCaptain = vehicleCaptain;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}


	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public TransportDetails copyDomainObject2Entity(BaseEntity entity) {

		TransportDetails transportDetails = null;

		if(entity == null){
			transportDetails = new TransportDetails();
		}else {
			transportDetails = (TransportDetails)entity;
		}
		transportDetails.setCreatedBy(this.getCreatedBy());
		transportDetails.setUpdatedBy(this.getUpdatedBy());
		transportDetails.setCreatedAt(this.getCreatedAt());
		transportDetails.setUpdatedAt(this.getUpdatedAt());

		transportDetails.setId(this.getId());
		transportDetails.setIndustryCode(this.getIndustryCode());
		transportDetails.setStockCode(this.getStockCode());
		transportDetails.setStockVarietyCode(this.getStockVarietyCode());
		transportDetails.setTransportTypeCode(this.getTransportTypeCode());
		transportDetails.setVehicleCaptain(this.getVehicleCaptain());
		transportDetails.setVehicleNumber(this.getVehicleNumber());
		transportDetails.setRstNumber(this.getRstNumber());
		transportDetails.setMoreDetails(this.getMoreDetails());
		return transportDetails;
	}

	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {

		TransportDetails transportDetails = (TransportDetails)entity;

		this.setCreatedBy(transportDetails.getCreatedBy());
		this.setUpdatedBy(transportDetails.getUpdatedBy());
		this.setCreatedAt(transportDetails.getCreatedAt());
		this.setUpdatedAt(transportDetails.getUpdatedAt());

		this.setId(transportDetails.getId());
		this.setStockCode(transportDetails.getStockCode());
		this.setStockVarietyCode(transportDetails.getStockVarietyCode());
		this.setIndustryCode(transportDetails.getIndustryCode());
		this.setVehicleCaptain(transportDetails.getVehicleCaptain());
		this.setVehicleNumber(transportDetails.getVehicleNumber());
		this.setRstNumber(transportDetails.getRstNumber());
		this.setMoreDetails(transportDetails.getMoreDetails());
		this.setMoreDetails(transportDetails.getMoreDetails());
	}

	@Override
	public String toString() {
		return "TransportDetailsDO{" +
				"id=" + id +
				", industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				", transportTypeCode='" + transportTypeCode + '\'' +
				", rstNumber='" + rstNumber + '\'' +
				", vehicleNumber='" + vehicleNumber + '\'' +
				", vehicleCaptain='" + vehicleCaptain + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}