/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package org.docstore.domainobjs.json.sms;

import com.docstore.model.BaseEntity;
import com.docstore.model.sms.PartyDetails;
import com.docstore.model.sms.StockDetails;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.docstore.domainobjs.json.BaseDomainObject;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * PartyDetailsDO .
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_EMPTY)
public class PartyDetailsDO extends BaseDomainObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String CLASS_NAME = PartyDetailsDO.class.getName();

	public PartyDetailsDO() {
	}

	public PartyDetailsDO(PartyDetails partyDetails) {
		copyEntity2DomainObject(partyDetails);
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("industryCode")
	private String industryCode;

	@JsonProperty("stockCode")
	private String stockCode;

	@JsonProperty("stockVarietyCode")
	private String stockVarietyCode;

	@JsonProperty("partyTypeCode")
	private String partyTypeCode;

	@JsonProperty("ownIndustryCode")
	private String ownIndustryCode;

	@JsonProperty("partyName1")
	private String partyName1;

	@JsonProperty("partyName2")
	private String partyName2;

	@JsonProperty("partyName3")
	private String partyName3;

	@JsonProperty("onBeHalf")
	private String onBeHalf;

	@JsonProperty("moreDetails")
	private String moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	public String getOnBeHalf() {
		return onBeHalf;
	}

	public void setOnBeHalf(String onBeHalf) {
		this.onBeHalf = onBeHalf;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getPartyTypeCode() {
		return partyTypeCode;
	}

	public void setPartyTypeCode(String partyTypeCode) {
		this.partyTypeCode = partyTypeCode;
	}

	public String getOwnIndustryCode() {
		return ownIndustryCode;
	}

	public void setOwnIndustryCode(String ownIndustryCode) {
		this.ownIndustryCode = ownIndustryCode;
	}

	public String getPartyName1() {
		return partyName1;
	}

	public void setPartyName1(String partyName1) {
		this.partyName1 = partyName1;
	}

	public String getPartyName2() {
		return partyName2;
	}

	public void setPartyName2(String partyName2) {
		this.partyName2 = partyName2;
	}

	public String getPartyName3() {
		return partyName3;
	}

	public void setPartyName3(String partyName3) {
		this.partyName3 = partyName3;
	}

	@JsonIgnore
	@Override
	public Object getDomainObjectKey() {
		return getId();
	}

	@Override
	public PartyDetails copyDomainObject2Entity(BaseEntity entity) {
		PartyDetails partyDetails = null;

		if(entity == null){
			partyDetails = new PartyDetails();
		}else {
			partyDetails = (PartyDetails)entity;
		}

		partyDetails.setCreatedBy(this.getCreatedBy());
		partyDetails.setUpdatedBy(this.getUpdatedBy());
		partyDetails.setCreatedAt(this.getCreatedAt());
		partyDetails.setUpdatedAt(this.getUpdatedAt());

		partyDetails.setId(this.getId());
		partyDetails.setStockCode(this.getStockCode());
		partyDetails.setStockVarietyCode(this.getStockVarietyCode());
		partyDetails.setIndustryCode(this.getIndustryCode());
		partyDetails.setPartyTypeCode(this.getPartyTypeCode());
		partyDetails.setPartyName1(this.getPartyName1());
		partyDetails.setPartyName2(this.getPartyName2());
		partyDetails.setPartyName3(this.getPartyName3());
		partyDetails.setOnBehalf(this.getOnBeHalf());
		partyDetails.setOwnIndustryCode(this.getOwnIndustryCode());
		partyDetails.setMoreDetails(this.getMoreDetails());

		return partyDetails;
	}
	@Override
	public void copyEntity2DomainObject(BaseEntity entity) {
		PartyDetails partyDetails = (PartyDetails)entity;
		this.setCreatedBy(partyDetails.getCreatedBy());
		this.setUpdatedBy(partyDetails.getUpdatedBy());
		this.setCreatedAt(partyDetails.getCreatedAt());
		this.setUpdatedAt(partyDetails.getUpdatedAt());

		this.setId(partyDetails.getId());
		this.setStockCode(partyDetails.getStockCode());
		this.setStockVarietyCode(partyDetails.getStockVarietyCode());
		this.setStockVarietyCode(partyDetails.getStockVarietyCode());
		this.setIndustryCode(partyDetails.getIndustryCode());
		this.setPartyTypeCode(partyDetails.getPartyTypeCode());
		this.setPartyName1(partyDetails.getPartyName1());
		this.setPartyName2(partyDetails.getPartyName2());
		this.setPartyName3(partyDetails.getPartyName3());
		this.setOnBeHalf(partyDetails.getOnBehalf());
		this.setOwnIndustryCode(partyDetails.getOwnIndustryCode());
		this.setMoreDetails(partyDetails.getMoreDetails());

	}

	@Override
	public String toString() {
		return "PartyDetailsDO{" +
				"id=" + id +
				", industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				", partyTypeCode='" + partyTypeCode + '\'' +
				", ownIndustryCode='" + ownIndustryCode + '\'' +
				", partyName1='" + partyName1 + '\'' +
				", partyName2='" + partyName2 + '\'' +
				", partyName3='" + partyName3 + '\'' +
				", onBeHalf='" + onBeHalf + '\'' +
				", moreDetails='" + moreDetails + '\'' +
				'}';
	}
}