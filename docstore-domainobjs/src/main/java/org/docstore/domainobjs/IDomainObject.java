package org.docstore.domainobjs;

import com.docstore.framework.IEntity;
import com.docstore.model.BaseEntity;
import org.docstore.domainobjs.json.TableRowMapDO;

public interface IDomainObject {
	public Object getDomainObjectKey();
	public BaseEntity copyDomainObject2Entity(BaseEntity entity);
	public void copyEntity2DomainObject(BaseEntity entity);
}
