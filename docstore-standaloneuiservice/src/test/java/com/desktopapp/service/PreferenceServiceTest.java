package com.desktopapp.service;

import com.desktopapp.service.config.AppConfig;
import com.desktopapp.service.config.JPAConfiguration;
import com.docstore.jpa.spring.extended.config.ExtendedRepositoryConfig;
import org.docstore.domainobjs.json.CodeDataItemDO;
import org.docstore.domainobjs.json.PreferencesDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by diwakar009 on 17/12/2018.
 */
/*@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class,ExtendedRepositoryConfig.class})*/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AppConfig.class})
//@Transactional
public class PreferenceServiceTest {
    @Autowired
    private PreferencesService preferencesService;

    //@Test
    public void testSampleService() {
        PreferencesDO preferencesDO = preferencesService.find(1l);
        System.out.println("*************** " +preferencesDO.getAppTheme());
    }

    @Test
    public void uppdatePreferenceService() {
        PreferencesDO preferencesDO = preferencesService.find(1l);
        preferencesDO.setShowTipOfTheDay(true);
        preferencesService.update(preferencesDO);
        preferencesDO = preferencesService.find(1l);

        System.out.println("############ " +preferencesDO.isShowTipOfTheDay());
    }

}
