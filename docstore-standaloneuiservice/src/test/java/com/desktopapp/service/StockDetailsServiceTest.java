/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service;

/**
 * Created by diwakar009 on 1/11/2018.
 */

import com.desktopapp.service.config.AppConfig;
import com.desktopapp.service.stockmaintenance.StockDetailsService;
import com.desktopapp.service.stockmaintenance.StockService;
import org.docstore.domainobjs.json.sms.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.AssertTrue;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AppConfig.class})
@Transactional
public class StockDetailsServiceTest {

    @Autowired
    private StockDetailsService stockDetailsService;

    @Autowired
    private StockService stockService;

    @Test
    @Rollback(true)
    public void testSaveStockDetails() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();

        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setIndustryCode("KMR");
        stockDetailsDO.setStockCode("PADD");
        stockDetailsDO.setStockVarietyCode("ODPY");


        try {
            calobj.setTime(df.parse("20/10/2019 00:00:00"));
            stockDetailsDO.setTransactionDate(calobj.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        /*stockDetailsDO.setTransactionDate(calobj.getTime());*/

        stockDetailsDO.setTransactionType("RETN"); // RETN
        stockDetailsDO.setPlusOrMinusOnTotal('-');
        stockDetailsDO.setNoOfPackets(100l);
        stockDetailsDO.setTotalWeightment(100l * 0.50d);
        stockDetailsDO.setRemarks("Test Save");

        PartyDetailsDO partyDetailsDO = new PartyDetailsDO();
        partyDetailsDO.setIndustryCode(stockDetailsDO.getIndustryCode());
        partyDetailsDO.setStockCode(stockDetailsDO.getStockCode());
        partyDetailsDO.setStockVarietyCode(stockDetailsDO.getStockVarietyCode());
        partyDetailsDO.setPartyTypeCode("MANDI");
        partyDetailsDO.setPartyName1("Mandi Name");
        partyDetailsDO.setPartyName2("Broker Name");
        partyDetailsDO.setPartyName3("Farmer Name");

        stockDetailsDO.setPartyDetailsDO(partyDetailsDO);

        TransportDetailsDO transportDetailsDO = new TransportDetailsDO();
        transportDetailsDO.setIndustryCode(stockDetailsDO.getIndustryCode());
        transportDetailsDO.setStockCode(stockDetailsDO.getStockCode());
        transportDetailsDO.setStockVarietyCode(stockDetailsDO.getStockVarietyCode());
        transportDetailsDO.setTransportTypeCode("OWN");
        transportDetailsDO.setRstNumber("1234");
        transportDetailsDO.setVehicleNumber("OR 10 H 7477");
        transportDetailsDO.setVehicleCaptain("Buchi");

        stockDetailsDO.setTransportDetailsDO(transportDetailsDO);
        stockDetailsDO = stockDetailsService.saveOrUpdateStockDetails(stockDetailsDO);
        StockDO stockDO = stockDetailsDO.getStockDO();

        System.out.println("*****************************************************************");
        System.out.println(stockDetailsDO.toString());
        System.out.println(stockDO.toString());
        System.out.println("*****************************************************************");

    }

    @Test
    @Rollback(true)
    public void testGetStockDetails(){
        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setIndustryCode("KMR");
        stockDetailsDO.setStockCode("PADD");
        stockDetailsDO.setStockVarietyCode("ODPY");
        stockDetailsDO.setId(40l);
        StockDetailsDO stockDetailsDO1 = stockDetailsService.find(stockDetailsDO);
        if(stockDetailsDO1 != null) {
            StockDO stockDO = stockDetailsDO1.getStockDO();
            PartyDetailsDO partyDetailsDO = stockDetailsDO1.getPartyDetailsDO();
            TransportDetailsDO transportDetailsDO = stockDetailsDO1.getTransportDetailsDO();
            DeliveryDetailsDO deliveryDetailsDO = stockDetailsDO1.getDeliveryDetailsDO();
            System.out.println("*****************************************************************");
            System.out.println(stockDO != null ? stockDO.toString() : "No Stock");
            System.out.println(partyDetailsDO != null ? partyDetailsDO.toString() : "No Party");
            System.out.println(transportDetailsDO != null ? transportDetailsDO.toString() : "No Transport");
            System.out.println(deliveryDetailsDO != null ? deliveryDetailsDO.toString() : "No Delivery Details");
            System.out.println("*****************************************************************");
        }
    }

    @Test
    @Rollback(true)
    public void testUpdateStockDetails() {

        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setId(12l);
        stockDetailsDO = stockDetailsService.find(stockDetailsDO);

        stockDetailsDO.setNoOfPackets(150l);
        stockDetailsDO.setTotalWeightment(150l * 0.50d);
        stockDetailsDO.setRemarks("Test Update");

        PartyDetailsDO partyDetailsDO = stockDetailsDO.getPartyDetailsDO();
        partyDetailsDO.setPartyName1("Mandi Name1");
        partyDetailsDO.setPartyName2("Broker Name1");
        partyDetailsDO.setPartyName3("Farmer Name1");
        stockDetailsDO.setPartyDetailsDO(partyDetailsDO);


        TransportDetailsDO transportDetailsDO = stockDetailsDO.getTransportDetailsDO();
        transportDetailsDO.setRstNumber("12347");
        transportDetailsDO.setVehicleNumber("OR 10 H 17477");
        transportDetailsDO.setVehicleCaptain("Buchi1");
        stockDetailsDO.setTransportDetailsDO(transportDetailsDO);

        stockDetailsDO = stockDetailsService.saveOrUpdateStockDetails(stockDetailsDO);
        StockDO stockDO = stockDetailsDO.getStockDO();

        System.out.println("*****************************************************************");
        System.out.println(stockDetailsDO.toString());
        System.out.println(stockDO.toString());
        System.out.println("*****************************************************************");

    }

    @Test
    @Rollback(true)
    public void testDeleteStockDetail(){
        StockDetailsDO stockDetailsDO = new StockDetailsDO();
        stockDetailsDO.setId(13l);
        boolean isDeleted = stockDetailsService.deleteStockDetails(stockDetailsDO);
        System.out.println("*****************************************************************");
        System.out.println(isDeleted);
        System.out.println("*****************************************************************");
    }
}
