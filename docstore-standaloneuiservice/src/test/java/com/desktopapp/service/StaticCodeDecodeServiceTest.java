package com.desktopapp.service;

/**
 * Created by diwakar009 on 1/11/2018.
 */

import com.desktopapp.service.config.AppConfig;
import com.desktopapp.service.config.JPAConfiguration;
import com.docstore.model.StaticCodeDecode;
import org.docstore.domainobjs.json.CodeDataItemDO;
import org.docstore.domainobjs.json.DOPage;
import org.docstore.domainobjs.json.StaticCodeDecodeDO;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AppConfig.class})
//@Transactional
public class StaticCodeDecodeServiceTest {

    @Autowired
    private StaticCodeDecodeService staticCodeDecodeService;

    @Test
    public void testSampleService() {
        Optional<List<CodeDataItemDO>> codeDataItemDOList = staticCodeDecodeService.getCodeDataItemList("CD_CURRENCY");
        if(codeDataItemDOList.isPresent()) {
            codeDataItemDOList.get().forEach(codedecode -> {
                System.out.println("######################## " + codedecode.toString());
            });

            Assert.assertEquals(7,codeDataItemDOList.get().size());
        }


    }

    @Test
    public void testListByCodeNameNLang(){
       Map<String, Object> parameters = new HashMap();
       parameters.put("page","0");
       parameters.put("size","10");
       parameters.put("codeName","CD_CURRENCY");
       parameters.put("language","EN_US");
       parameters.put("filter","%");

       DOPage<TableRowMapDO<String, String>> page = staticCodeDecodeService.listByCodeNameNLang(parameters);
       System.out.println(page.toString());
    }


}
