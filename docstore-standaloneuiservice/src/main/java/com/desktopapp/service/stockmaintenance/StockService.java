/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.service.AbstractService;
import com.docstore.jpa.spring.extended.StockRepository;
import com.docstore.model.BaseEntity;
import com.docstore.model.sms.Stock;
import com.docstore.model.sms.StockPK;
import org.docstore.domainobjs.IDomainObject;
import org.docstore.domainobjs.json.StaticCodeDecodeDO;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.StockDO;
import org.docstore.domainobjs.json.sms.StockDetailsDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Stock Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class StockService extends AbstractService<StockDO> {

    @Resource
    protected StockRepository stockRepository;


    public StockService() {
        super(StockDO.class);
    }

    @Override
    public Class<Stock> getEntityClass() {
        return Stock.class;
    }


    public Stock findStockNLock(String industryCode, String stockCode, String stockVarietyCode){

        Stock stock =  getRepository().findStockNLock(industryCode,stockCode,stockVarietyCode);

        return stock;
    }

    /**
     * Save or update stock entry
     *
     * @param updatedStockDo
     * @return
     */
    public StockDO saveOrUpdateStock(StockDO updatedStockDo){

        Stock stock = findStockNLock(updatedStockDo.getIndustryCode(),updatedStockDo.getStockCode(),updatedStockDo.getStockVarietyCode());
        if(stock == null){
            stock = new Stock(new StockPK(updatedStockDo.getIndustryCode(),updatedStockDo.getStockCode(),updatedStockDo.getStockVarietyCode()));
        }
        updatedStockDo.copyDomainObject2Entity(stock);

        stock = getRepository().save(stock);

        return new StockDO(stock);
    }

    /**
     * Save and update stock totals
     *  1) Increment or decrement the total no of packets and total weightment from the stock table
     *  2) Save the stock details
     *
     * @param stockDetailsDO
     * @return
     */
    public StockDO updateStockTotals(StockDetailsDO stockDetailsDO){
        /**
         * Update the Stock record with the total number of packets and total weightment in quintals
         */
        Stock stock = findStockNLock(stockDetailsDO.getIndustryCode(), stockDetailsDO.getStockCode(), stockDetailsDO.getStockVarietyCode());
        if (stock == null) {
            stock = new Stock(new StockPK(stockDetailsDO.getIndustryCode(), stockDetailsDO.getStockCode(), stockDetailsDO.getStockVarietyCode()));
        }

        if (stockDetailsDO.getPlusOrMinusOnTotal().equals('+')) {

            stock.addTotalNoofPackets(stockDetailsDO.getNoOfPackets());
            stock.addTotalWeightment(stockDetailsDO.getTotalWeightment());

        } else if (stockDetailsDO.getPlusOrMinusOnTotal().equals('-')) {

            stock.substractTotalNoofPackets(stockDetailsDO.getNoOfPackets());
            stock.substractTotalWeightment(stockDetailsDO.getTotalWeightment());

        }

        StockDO updatedStockDO = (StockDO) update(new StockDO(stock));
        return updatedStockDO;
    }

    /**
     * Revise the stock totals by replacing old stock details with new stock details
     *      1) Reverse the old totals on over all stock totals
     *      2) apply the new totals to over all stock totals
     *
     *
     * @param newStockDetails
     * @param oldStockDetails
     * @return
     */
    public StockDO reviseStockTotals(StockDetailsDO newStockDetails,StockDetailsDO oldStockDetails){

        Stock stock = findStockNLock(newStockDetails.getIndustryCode(), newStockDetails.getStockCode(), newStockDetails.getStockVarietyCode());
        // reverse old number of packets and total weightment
        if (oldStockDetails.getPlusOrMinusOnTotal().equals('+')) {

            stock.substractTotalNoofPackets(oldStockDetails.getNoOfPackets());
            stock.substractTotalWeightment(oldStockDetails.getTotalWeightment());

        } else if (oldStockDetails.getPlusOrMinusOnTotal().equals('-')) {

            stock.addTotalNoofPackets(oldStockDetails.getNoOfPackets());
            stock.addTotalWeightment(oldStockDetails.getTotalWeightment());

        }
        // apply the new totals

        if (newStockDetails.getPlusOrMinusOnTotal().equals('+')) {

            stock.addTotalNoofPackets(newStockDetails.getNoOfPackets());
            stock.addTotalWeightment(newStockDetails.getTotalWeightment());

        } else if (newStockDetails.getPlusOrMinusOnTotal().equals('-')) {

            stock.substractTotalNoofPackets(newStockDetails.getNoOfPackets());
            stock.substractTotalWeightment(newStockDetails.getTotalWeightment());

        }

        StockDO updatedStockDO = (StockDO) update(new StockDO(stock));
        return updatedStockDO;
    }

    /**
     * Reverse Stock totals.
     *
     * Reverse the stock entries from the totals
     *
     * @param stockDetailsDO
     * @return
     */
    public StockDO reverseStockTotals(StockDetailsDO stockDetailsDO){

        Stock stock = findStockNLock(stockDetailsDO.getIndustryCode(), stockDetailsDO.getStockCode(), stockDetailsDO.getStockVarietyCode());
        // reverse old number of packets and total weightment
        if (stockDetailsDO.getPlusOrMinusOnTotal().equals('+')) {

            stock.substractTotalNoofPackets(stockDetailsDO.getNoOfPackets());
            stock.substractTotalWeightment(stockDetailsDO.getTotalWeightment());

        } else if (stockDetailsDO.getPlusOrMinusOnTotal().equals('-')) {

            stock.addTotalNoofPackets(stockDetailsDO.getNoOfPackets());
            stock.addTotalWeightment(stockDetailsDO.getTotalWeightment());

        }
        StockDO updatedStockDO = (StockDO) update(new StockDO(stock));
        return updatedStockDO;
    }

    @Override
    public StockDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        StockDO domainObject = new StockDO((Stock) entity);
        return domainObject;
    }

    @Override
    public List<StockDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<StockDO> domainObjects = new ArrayList<StockDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                StockDO domainObject = new StockDO((Stock) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(StockDO domainObject) {
        StockDO stockDO = (StockDO) domainObject;
        Stock stock = new Stock();
        stockDO.copyDomainObject2Entity(stock);
        return stock;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<StockDO> domainObjects) {
        List<Stock> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            StockDO stockDO = (StockDO) domainObject;
            Stock stock = new Stock();
            stockDO.copyDomainObject2Entity(stock);
            entities.add(stock);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        StockDO code = (StockDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", code.getId().toString());
        tableRowMapDO.setClazzName(StaticCodeDecodeDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public StockRepository getRepository() {
        return this.stockRepository;
    }
}
