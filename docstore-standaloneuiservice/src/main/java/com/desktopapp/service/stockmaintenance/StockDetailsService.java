/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.service.AbstractService;
import com.docstore.jpa.spring.extended.StockDetailsRepository;
import com.docstore.jpa.spring.extended.StockRepository;
import com.docstore.model.BaseEntity;
import com.docstore.model.sms.Stock;
import com.docstore.model.sms.StockDetails;
import com.docstore.model.sms.StockPK;
import com.docstore.util.DateTimeUtil;
import org.docstore.domainobjs.IDomainObject;
import org.docstore.domainobjs.json.DOPage;
import org.docstore.domainobjs.json.StaticCodeDecodeDO;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.StockDO;
import org.docstore.domainobjs.json.sms.StockDOPK;
import org.docstore.domainobjs.json.sms.StockDetailsDO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * Stock Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class StockDetailsService extends AbstractService<StockDetailsDO> {

    @Resource
    private StockDetailsRepository stockDetailsRepository;

    @Resource
    private StockService stockService;

   /*
    @Resource
    private PartyDetailsService partyDetailsService;

    @Resource
    private DeliveryDetailsService deliveryDetailsService;

    @Resource
    private TransportDetailsService transportDetailsService;

    @Resource
    private ContractService contractService;
    */


    public StockDetailsService() {
        super(StockDetailsDO.class);
    }

    @Override
    public Class<StockDetails> getEntityClass() {
        return StockDetails.class;
    }


    @Override
    public Page<? extends BaseEntity> findByFilter(Map<String, Object> parameters, PageRequest pageRequest) {
        String industryCodeFilter = (String)parameters.get("industryCode");
        String stockCodeFilter = (String)parameters.get("stockCode");
        String stockVarietyCodeFilter = (String)parameters.get("stockVarietyCode");
        String transTypeFilter = (String)parameters.get("transactionType");
        String rstNumberFilter = (String)parameters.get("rstNumber");
        String vehicleNumberFilter = (String)parameters.get("vehicleNumber");
        Date startDateFilter = (Date)parameters.get("transStartDate");
        Date endDateFilter = (Date)parameters.get("transEndDate");
        return getRepository().findAllStockDetailsByFilter(industryCodeFilter,stockCodeFilter,stockVarietyCodeFilter,transTypeFilter,rstNumberFilter,vehicleNumberFilter,startDateFilter,endDateFilter,pageRequest);
    }

    @Override
    public Page<? extends BaseEntity> findAll(Map<String, Object> parameters,PageRequest pageRequest){
        /*return getRepository().findAllStockDetails(pageRequest);*/
        return findByFilter(new HashMap<String, Object>(),pageRequest);
    }

    /**
     * Save or update the stock details
     *
     *
     * @param newStockDetails
     */
    @Transactional
    public StockDetailsDO saveOrUpdateStockDetails(StockDetailsDO newStockDetails){

        boolean isUpdate = false;
        StockDetailsDO oldStockDetails = find(newStockDetails); // find old stock details if any using ID
        if(oldStockDetails != null){
            isUpdate = true;
        }


        StockDetailsDO updatedStockDetailsDo = (StockDetailsDO)update(newStockDetails);

        if(isUpdate) {
            // reverse the totals of the stock table
            // apply the new value to the stock table
            boolean needTotalsUpdate = false;

            if(!newStockDetails.getPlusOrMinusOnTotal().equals(oldStockDetails.getPlusOrMinusOnTotal())
                    || !newStockDetails.getNoOfPackets().equals(oldStockDetails.getNoOfPackets())
                    || !newStockDetails.getTotalWeightment().equals(oldStockDetails.getTotalWeightment())){

                needTotalsUpdate = true;
            }

            if(needTotalsUpdate){
                StockDO updatedStockDO = stockService.reviseStockTotals(newStockDetails,oldStockDetails);
                updatedStockDetailsDo.setStockDO(updatedStockDO);
            }

        }else {
            StockDO updatedStockDO = stockService.updateStockTotals(newStockDetails) ;
            updatedStockDetailsDo.setStockDO(updatedStockDO);
        }

        return updatedStockDetailsDo;
    }

    /**
     * Delete stock details
     *
     *  1) Delete the stock details records
     *  2) reverse the stock totals
     *
     * @param stockDetails
     * @return
     */
    public boolean deleteStockDetails(StockDetailsDO stockDetails){
        StockDetailsDO deleteableStockDetails = find(stockDetails); // find old stock details if any using ID
        if(deleteableStockDetails != null) {
            stockService.reverseStockTotals(deleteableStockDetails);
            remove(deleteableStockDetails);
            return true;
        }
        return false;
    }

    @Override
    public StockDetailsDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        StockDetailsDO domainObject = new StockDetailsDO((StockDetails)entity);
        return domainObject;
    }

    @Override
    public List<StockDetailsDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<StockDetailsDO> domainObjects = new ArrayList<StockDetailsDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                StockDetailsDO domainObject = new StockDetailsDO((StockDetails) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(StockDetailsDO domainObject) {
        StockDetailsDO stockDetailsDO = (StockDetailsDO) domainObject;
        StockDetails stockDetails = new StockDetails();
        stockDetailsDO.copyDomainObject2Entity(stockDetails);
        return stockDetails;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<StockDetailsDO> domainObjects) {
        List<StockDetails> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            StockDetailsDO stockDetailsDO = (StockDetailsDO) domainObject;
            StockDetails stockDetails = new StockDetails();
            stockDetailsDO.copyDomainObject2Entity(stockDetails);
            entities.add(stockDetails);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        StockDetailsDO stockDetailsDO = (StockDetailsDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", stockDetailsDO.getId().toString());
        tableRowMapDO.add("industryCode",stockDetailsDO.getIndustryCode());
        tableRowMapDO.add("stockCode",stockDetailsDO.getStockCode());
        tableRowMapDO.add("stockVarietyCode",stockDetailsDO.getStockVarietyCode());
        tableRowMapDO.add("transactionType",stockDetailsDO.getTransactionType());
        tableRowMapDO.add("numberOfPackets",String.valueOf(stockDetailsDO.getNoOfPackets()));
        tableRowMapDO.add("weightment",String.valueOf(stockDetailsDO.getTotalWeightment()));

        if(stockDetailsDO.getTransportDetailsDO() != null) {
            tableRowMapDO.add("rstNumber", stockDetailsDO.getTransportDetailsDO().getRstNumber());
            tableRowMapDO.add("vehicleNumber", stockDetailsDO.getTransportDetailsDO().getVehicleNumber());
        }

        tableRowMapDO.add("tranactionDate",String.valueOf(DateTimeUtil.convertDateToUTCString(stockDetailsDO.getTransactionDate())));

        tableRowMapDO.setClazzName(StockDetailsDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public StockDetailsRepository getRepository() {
        return this.stockDetailsRepository;
    }
}
