/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.service.AbstractService;
import com.docstore.jpa.spring.extended.ContractRepository;
import com.docstore.jpa.spring.extended.PartyDetailsRepository;
import com.docstore.model.BaseEntity;
import com.docstore.model.sms.Contract;
import com.docstore.model.sms.PartyDetails;
import org.docstore.domainobjs.IDomainObject;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.ContractDO;
import org.docstore.domainobjs.json.sms.PartyDetailsDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Contract Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class ContractService extends AbstractService<ContractDO> {

    @Resource
    protected ContractRepository contractRepository;

    public ContractService() {
        super(ContractDO.class);
    }

    @Override
    public Class<Contract> getEntityClass() {
        return Contract.class;
    }

    @Override
    public ContractDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        ContractDO domainObject = new ContractDO((Contract) entity);
        return domainObject;
    }

    @Override
    public List<ContractDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<ContractDO> domainObjects = new ArrayList<ContractDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                ContractDO domainObject = new ContractDO((Contract) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(ContractDO domainObject) {
        ContractDO contractDO = (ContractDO) domainObject;
        Contract contract = new Contract();
        contractDO.copyDomainObject2Entity(contract);
        return contract;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<ContractDO> domainObjects) {
        List<Contract> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            ContractDO contractDO = (ContractDO) domainObject;
            Contract contract = new Contract();
            contractDO.copyDomainObject2Entity(contract);
            entities.add(contract);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        ContractDO partyDetailsDO = (ContractDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", partyDetailsDO.getId().toString());
        tableRowMapDO.setClazzName(ContractDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public ContractRepository getRepository() {
        return this.contractRepository;
    }
}
