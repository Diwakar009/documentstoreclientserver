/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.service.AbstractService;
import com.docstore.jpa.spring.extended.PartyDetailsRepository;
import com.docstore.jpa.spring.extended.TransportDetailsRepository;
import com.docstore.model.BaseEntity;
import com.docstore.model.sms.PartyDetails;
import com.docstore.model.sms.TransportDetails;
import org.docstore.domainobjs.IDomainObject;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.PartyDetailsDO;
import org.docstore.domainobjs.json.sms.TransportDetailsDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Party Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class TransportDetailsService extends AbstractService<TransportDetailsDO> {

    @Resource
    protected TransportDetailsRepository transportDetailsRepository;

    public TransportDetailsService() {
        super(TransportDetailsDO.class);
    }

    @Override
    public Class<TransportDetails> getEntityClass() {
        return TransportDetails.class;
    }

    @Override
    public TransportDetailsDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        TransportDetailsDO domainObject = new TransportDetailsDO((TransportDetails)entity);
        return domainObject;
    }

    @Override
    public List<TransportDetailsDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<TransportDetailsDO> domainObjects = new ArrayList<TransportDetailsDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                TransportDetailsDO domainObject = new TransportDetailsDO((TransportDetails) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(TransportDetailsDO domainObject) {
        TransportDetailsDO transportDetailsDO = (TransportDetailsDO) domainObject;
        TransportDetails transportDetails = new TransportDetails();
        transportDetailsDO.copyDomainObject2Entity(transportDetails);
        return transportDetails;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<TransportDetailsDO> domainObjects) {
        List<TransportDetails> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            TransportDetailsDO transportDetailsDO = (TransportDetailsDO) domainObject;
            TransportDetails transportDetails = new TransportDetails();
            transportDetailsDO.copyDomainObject2Entity(transportDetails);
            entities.add(transportDetails);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        TransportDetailsDO partyDetailsDO = (TransportDetailsDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", partyDetailsDO.getId().toString());
        tableRowMapDO.setClazzName(TransportDetailsDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public TransportDetailsRepository getRepository() {
        return this.transportDetailsRepository;
    }
}
