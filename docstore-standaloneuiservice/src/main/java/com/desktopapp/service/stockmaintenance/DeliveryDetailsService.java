/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.desktopapp.service.stockmaintenance;


import com.desktopapp.service.AbstractService;
import com.docstore.jpa.spring.extended.DeliveryDetailsRepository;
import com.docstore.jpa.spring.extended.TransportDetailsRepository;
import com.docstore.model.BaseEntity;
import com.docstore.model.sms.DeliveryDetails;
import com.docstore.model.sms.TransportDetails;
import org.docstore.domainobjs.IDomainObject;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.docstore.domainobjs.json.sms.DeliveryDetailsDO;
import org.docstore.domainobjs.json.sms.TransportDetailsDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Party Details Service
 *
 * @author Ratnala Diwakar Choudhury
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class DeliveryDetailsService extends AbstractService<DeliveryDetailsDO> {

    @Resource
    protected DeliveryDetailsRepository deliveryDetailsRepository;

    public DeliveryDetailsService() {
        super(DeliveryDetailsDO.class);
    }

    @Override
    public Class<DeliveryDetails> getEntityClass() {
        return DeliveryDetails.class;
    }

    @Override
    public DeliveryDetailsDO returnDomainObject(BaseEntity entity) {

        if (entity == null) {
            return null;
        }

        DeliveryDetailsDO domainObject = new DeliveryDetailsDO((DeliveryDetails)entity);
        return domainObject;
    }

    @Override
    public List<DeliveryDetailsDO> returnDomainObjects(List<? extends BaseEntity> entities) {
        if (entities == null) {
            return null;
        }
        List<DeliveryDetailsDO> domainObjects = new ArrayList<DeliveryDetailsDO>();

        if (entities != null) {
            entities.forEach(entity -> {
                DeliveryDetailsDO domainObject = new DeliveryDetailsDO((DeliveryDetails) entity);
                domainObjects.add(domainObject);
            });
        }
        return domainObjects;
    }

    @Override
    public BaseEntity returnEntity(DeliveryDetailsDO domainObject) {
        DeliveryDetailsDO deliveryDetailsDO = (DeliveryDetailsDO) domainObject;
        DeliveryDetails deliveryDetails = new DeliveryDetails();
        deliveryDetailsDO.copyDomainObject2Entity(deliveryDetails);
        return deliveryDetails;
    }

   @Override
    public List<? extends BaseEntity> returnEntities(List<DeliveryDetailsDO> domainObjects) {
        List<DeliveryDetails> entities = new ArrayList();
        for (IDomainObject domainObject : domainObjects) {
            DeliveryDetailsDO deliveryDetailsDO = (DeliveryDetailsDO) domainObject;
            DeliveryDetails deliveryDetails = new DeliveryDetails();
            deliveryDetailsDO.copyDomainObject2Entity(deliveryDetails);
            entities.add(deliveryDetails);
        }
        return entities;
    }

    @Override
    public TableRowMapDO<String, String> copyDomainObjToTableRowMap(IDomainObject domainObject) {
        DeliveryDetailsDO deliveryDetailsDO = (DeliveryDetailsDO) domainObject;
        TableRowMapDO<String, String> tableRowMapDO = new TableRowMapDO<String, String>();
        tableRowMapDO.add("id", deliveryDetailsDO.getId().toString());
        tableRowMapDO.setClazzName(DeliveryDetailsDO.CLASS_NAME);
        return tableRowMapDO;
    }

    @Override
    public DeliveryDetailsRepository getRepository() {
        return this.deliveryDetailsRepository;
    }
}
