package com.desktopapp.service;

import com.desktopapp.jsonobject.FunctionAccessJSON;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Service
public class FunctionAccessService {

    public static final String FUNCTION_ACCESS_JSON_FILE = "functionAcccess.json";

    /**
     * returns function names based on access level
     * <p>
     * (*) returns all , which is super admin
     * <p>
     * Others as declared in json
     *
     * @param accessLevel
     * @return
     */
    public List<String> getFunctionAccessList(String accessLevel) {
        List<String> functionList = new ArrayList<String>();
        InputStream inputStream = FunctionAccessService.class.getClassLoader().getResourceAsStream(FUNCTION_ACCESS_JSON_FILE);

        try {

            if (inputStream != null) {
                StringWriter functionAccJSONStr = new StringWriter();
                IOUtils.copy(inputStream, functionAccJSONStr, "UTF-8");
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                FunctionAccessJSON functionAccessJSON = gson.fromJson(functionAccJSONStr.toString(), FunctionAccessJSON.class);

                if (functionAccessJSON != null) {
                    functionList = functionAccessJSON.getFunctionAccessMap().get(accessLevel);

                    if (functionList.contains("*")) {
                        return functionAccessJSON.getFullFunctionList();
                    } else {
                        return functionList;
                    }
                }

            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return functionList;
    }

}
