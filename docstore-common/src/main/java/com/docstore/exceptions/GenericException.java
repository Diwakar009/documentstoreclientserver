package com.docstore.exceptions;

/**
 * Created by diwakar009 on 10/12/2018.
 */
public class GenericException extends Exception{

    public GenericException() {
    }

    public GenericException(String s) {
        super(s);
    }

    public String displayException() {
        printStackTrace();
        return super.getMessage();
    }
}
