/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.component.constants;

/**
 * Created by diwakar009 on 21/10/2019.
 */
public class CodeDecodeConstants {
    public static final String CD_INDUSTRY_CODE = "CD_INDUSTRY_CODE";
    public static final String CD_STOCK_CODE = "CD_STOCK_CODE";
    public static final String CD_STOCK_VARIETY = "CD_STOCK_VARIETY";
    public static final String CD_TRANSACTION_TYPE = "CD_TRANS_TYPE";
    public static final String CD_LANGUAGE="CD_LANGUAGE";

    public static final String DEFAULT_LANGUAGE = "EN_US";

}
