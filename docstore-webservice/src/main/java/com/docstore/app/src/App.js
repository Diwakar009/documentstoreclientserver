import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

   state = {
    isLoading: true,
    staticCodeDecodes: []
  };

   async componentDidMount() {
    const response = await fetch('/api/staticCodeDecodes/CD_FILETYPE/EN_US');
    const body = await response.json();
    this.setState({ staticCodeDecodes: body, isLoading: false });
  }

  render() {
   const {staticCodeDecodes, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    return (
      <div className="App">
           <header className="App-header">
           <img src={logo} className="App-logo" alt="logo" />
           <div className="App-intro">
            <h2>Static Code Decode List</h2>
                  {staticCodeDecodes.map(staticCodeDecode =>
                        <div key={staticCodeDecode.id.codeName}> {staticCodeDecode.codeDesc}</div>)
                  }
           </div>
           </header>
      </div>

    );
  }
}

export default App;
