package com.docstore.ws.web.api;

import java.util.List;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.AppUserLoginService;
import com.docstore.framework.IEntity;

import org.docstore.domainobjs.json.AppUserLoginDO;
import org.docstore.domainobjs.json.TableRowMapDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.docstore.model.AppUserLogin;

@RestController
@RequestMapping(value="/api/appUserLogin")
public class AppUserLoginController extends GenericBaseController<AppUserLoginDO> {
	
	@Autowired
	AppUserLoginService service;

	@Override
	public AppUserLoginDO save(AppUserLoginDO appUserLoginDO) {
		return  (AppUserLoginDO)service.create(appUserLoginDO);
	}

	@Override
	public boolean delete(AppUserLoginDO appUserLoginDO) {
		service.remove(appUserLoginDO);
		return true;
	}

	@Override
	public boolean deleteList(List<AppUserLoginDO> doList) {
		service.remove(doList);
		return true;
	}

	@Override
	public AppUserLoginDO find(AppUserLoginDO appUserLoginDO) {
		return service.find(appUserLoginDO);
	}

	@Override
	public AbstractService getService() {
		return service;
	}


	@RequestMapping(
			value = "/appUserLoginByUserName",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppUserLoginDO> appUserLoginByUsername(@RequestParam("userName") String username) {
		logger.info("> appUserLoginByUsername");
		AppUserLoginDO appUserLoginDO = ((AppUserLoginService)service).getAppUserLoginByUserName(username);
		logger.info("< appUserLoginByUsername");
		return  new ResponseEntity<AppUserLoginDO>(appUserLoginDO, HttpStatus.OK);
	}
}
