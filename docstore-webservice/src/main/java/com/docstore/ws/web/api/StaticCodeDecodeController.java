package com.docstore.ws.web.api;

import java.util.List;

import com.desktopapp.service.AbstractService;
import com.desktopapp.service.StaticCodeDecodeService;
import com.docstore.framework.IEntity;
import org.docstore.domainobjs.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.docstore.model.StaticCodeDecode;

@RequestMapping(value="/api/staticCodeDecodes")
@RestController
public class StaticCodeDecodeController extends GenericBaseController<StaticCodeDecodeDO> {


	/**
	 * The StaticCodeDecodeService business service.
	 */
	@Autowired
	private StaticCodeDecodeService service;




	/**
	 * Web service endpoint to fetch CodeDataItems entities. The service returns
	 * the collection of StaticCodeDecode entities as JSON (Used).
	 *
	 * @return A ResponseEntity containing a Collection of CodeDataItemDO objects.
	 */
	@RequestMapping(
			value = "/{codeName}/{language}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)


	public ResponseEntity<List<CodeDataItemDO>> getStaticCodeDataItemList(@PathVariable("codeName")String codeName
			,@PathVariable("language")String language) {

		logger.info("> getStaticCodeDataItemList ");

		List<CodeDataItemDO> staticCodeDecodes = service.getStaticCodeDataItemList(codeName,language);

		logger.info("< getStaticCodeDataItemList ");
		return new ResponseEntity<List<CodeDataItemDO>>(staticCodeDecodes,HttpStatus.OK);
	}


	@Override
	public StaticCodeDecodeDO save(StaticCodeDecodeDO staticCodeDecodeDO) {
		return (StaticCodeDecodeDO)service.create(staticCodeDecodeDO);
	}

	@Override
	public boolean delete(StaticCodeDecodeDO staticCodeDecodeDO){
		service.remove(staticCodeDecodeDO);
		return true;
	}

	@Override
	public boolean deleteList(List<StaticCodeDecodeDO> doList) {
		service.remove(doList);
		return true;
	}

	@Override
	public StaticCodeDecodeDO find(StaticCodeDecodeDO staticCodeDecodeDO) {
		return service.find(staticCodeDecodeDO);
	}

	@Override
	public AbstractService getService() {
		return service;
	}


}
