package com.docstore.ws.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.desktopapp.service","com.docstore.jpa.spring.extended"})
/*@ContextConfiguration(locations = {
        "classpath:applicationContext-service.xml"})*/
public class AppConfig {

}