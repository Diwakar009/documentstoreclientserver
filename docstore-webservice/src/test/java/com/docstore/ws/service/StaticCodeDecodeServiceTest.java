package com.docstore.ws.service;

import java.util.List;

import com.desktopapp.service.StaticCodeDecodeService;
import com.docstore.ws.BaseUnitTest;
import com.docstore.ws.config.AppConfig;
import org.docstore.domainobjs.json.CodeDataItemDO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.docstore.model.StaticCodeDecode;

/**
 * Unit test methods for the GreetingService and GreetingServiceBean.
 * 
 * @author Diwakar Choudhury
 */



@Transactional
public class StaticCodeDecodeServiceTest extends BaseUnitTest {

	@Autowired
	private StaticCodeDecodeService staticCodeDecodeService;

    @Before
    public void setUp() {
        System.out.println("*****************START TEST*******************");

    }

    @After
    public void tearDown() {
        System.out.println("*****************END TEST*******************");
    }

    
    @Test
    public void testGetStaticCodeDecodeListWithLang(){
    	
    	List<CodeDataItemDO> codeDataItems = staticCodeDecodeService.getCodeDataItemList("EN_US");

    	Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.size() > 0);

        for (CodeDataItemDO scd : codeDataItems){
            displayMessage(scd.toString());
        }

    }

    @Test
    public void testGetStaticCodeDecodeListWithCodeNameNLang(){

        List<CodeDataItemDO> codeDataItems = staticCodeDecodeService.getStaticCodeDataItemList("CD_ACCESSLEVEL","EN_US");

        Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.size() > 0);

        for (CodeDataItemDO scd : codeDataItems){
            displayMessage(scd.toString());
        }

    }

    @Test
    public void testGetStaticCodeDecodeListWithCodeNameCodeValueNDesc(){

        List<CodeDataItemDO> codeDataItems = staticCodeDecodeService.getStaticCodeDataItemList("CD_ACCESSLEVEL","EN_US");

        Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.size() > 0);

        for (CodeDataItemDO scd : codeDataItems){
            displayMessage(scd.toString());
        }

    }

    @Test
    public void testGetStaticCodeDecodeListCodeNameNLang4rDropDown(){

        List<CodeDataItemDO> codeDataItems = staticCodeDecodeService.getStaticCodeDataItemList("CD_ACCESSLEVEL","EN_US");

        Assert.assertTrue(
                "Number of code decode Retrevived :",
                codeDataItems.size() > 0);

        for (CodeDataItemDO scd : codeDataItems){
            displayMessage(scd.toString());
        }

    }

    /*@Test
    public void testGetStaticCodeDecodeListCodeNameNCodeValuePageable(){

        PageRequest request = new PageRequest(2,2);

        Page<StaticCodeDecode> page = staticCodeDecodeService.getStaticCodeDecodeList("%Admin%","%Admin%","%Admin%",request);


        Assert.assertTrue(
                "Number of code decode Retrevived :",
                page.getTotalElements() == 1);


        displayMessage("Total Number of Elements :" + page.getTotalElements());
        displayMessage("Total Number of Pages :" + page.getTotalPages());
        displayMessage("Size :" + page.getSize());
        displayMessage("Number of Elements :" + page.getNumberOfElements());
        for (StaticCodeDecode scd : (List<StaticCodeDecode>)page.getContent()){
            displayMessage(scd.toString());
        }

    }*/

    
}

