USE docstore_desktopdb;

DROP TABLE IF EXISTS TBL_INDUSTRY;

CREATE TABLE TBL_INDUSTRY (
	INDUSTRY_CODE VARCHAR(5), -- CD_INDUSTRY_CODE
	INDUSTRY_TYPE VARCHAR(5), -- CD_INDUSTRY_TYPE_CODE
	INDUSTRY_OWNER VARCHAR(40),
	INDUSTRY_STATUS VARCHAR(5), -- CD_INDUSTRY_STATUS
	MORE_DETAILS TEXT,
	PRIMARY KEY (INDUSTRY_CODE)
);

ALTER TABLE TBL_INDUSTRY ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_INDUSTRY ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_INDUSTRY ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_INDUSTRY ADD  updated_at DATETIME DEFAULT NULL;


DROP TABLE IF EXISTS TBL_STOCK;

CREATE TABLE TBL_STOCK (
	INDUSTRY_CODE VARCHAR(5), -- CD_INDUSTRY_CODE
	STOCK_CODE VARCHAR(5), -- CD_STOCK_CODE
	STOCK_VARIETIES_CODE VARCHAR(5), -- CD_STOCK_VARIETY
	TOTAL_NO_OF_PKTS BIGINT(6),
	TOTAL_WEIGHTMENT DECIMAL(10,2),
    AS_ON_DATE DATETIME,
    MORE_DETAILS TEXT,
	PRIMARY KEY (INDUSTRY_CODE,STOCK_CODE,STOCK_VARIETIES_CODE)
);

ALTER TABLE TBL_STOCK ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_STOCK ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_STOCK ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_STOCK ADD  updated_at DATETIME DEFAULT NULL;

DROP TABLE IF EXISTS TBL_STOCK_DTLS;

CREATE TABLE TBL_STOCK_DTLS(
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    INDUSTRY_CODE VARCHAR(5),-- CD_INDUSTRY_CODE
	STOCK_CODE VARCHAR(5),-- CD_STOCK_CODE
	STOCK_VARIETIES_CODE VARCHAR(5),-- CD_STOCK_VARIETY
	TRANSACTION_TYPE VARCHAR(5), -- CD_TRANS_TYPE
	TRANSACTION_DATE DATETIME,
	NO_OF_PACKETS BIGINT(6),
	TOTAL_WEIGHTMENT DECIMAL(10,2),
	PLUS_OR_MINUS_ON_TOTAL CHAR(1),
	PARTY_ID BIGINT,
    TRANSPORT_ID BIGINT,
    DELIVERY_ID BIGINT,
    REMARKS VARCHAR(100),
    ONBEHALF VARCHAR(5),
    MORE_DETAILS TEXT,
    PRIMARY KEY (ID)
);

ALTER TABLE TBL_STOCK_DTLS ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_STOCK_DTLS ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_STOCK_DTLS ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_STOCK_DTLS ADD  updated_at DATETIME DEFAULT NULL;

DROP TABLE IF EXISTS TBL_PARTY_DTLS;

CREATE TABLE TBL_PARTY_DTLS (
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	INDUSTRY_CODE VARCHAR(5),-- CD_INDUSTRY_CODE
	STOCK_CODE VARCHAR(40),-- CD_STOCK_CODE
	STOCK_VARIETIES_CODE VARCHAR(40),-- CD_STOCK_VARIETY
	PARTY_TYPE_CODE VARCHAR(5), -- CD_PARTY_TYPE
	OWN_INDUSTRY_CODE VARCHAR(5), -- CD_INDUSTRY_CODE
	PARTY_NAME_1 VARCHAR(50),
	PARTY_NAME_2 VARCHAR(50),
	PARTY_NAME_3 VARCHAR(50),
	MORE_DETAILS TEXT,
    PRIMARY KEY (ID)
);

ALTER TABLE TBL_PARTY_DTLS ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_PARTY_DTLS ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_PARTY_DTLS ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_PARTY_DTLS ADD  updated_at DATETIME DEFAULT NULL;

DROP TABLE IF EXISTS TBL_TRANSPORT_DTLS;

CREATE TABLE TBL_TRANSPORT_DTLS(
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	INDUSTRY_CODE VARCHAR(5), -- CD_INDUSTRY_CODE
	STOCK_CODE VARCHAR(40),-- CD_STOCK_CODE
	STOCK_VARIETIES_CODE VARCHAR(40),-- CD_STOCK_VARIETY
	TRANSPORT_TYPE_CODE VARCHAR(5), -- CD_TRANSPORT_TYPE
	VEHICLE_NUMBER VARCHAR(20),
	VEHICLE_CAPTAIN VARCHAR(40),
	RST_NUMBER VARCHAR(20),
	MORE_DETAILS TEXT,
    PRIMARY KEY (ID)
);

ALTER TABLE TBL_TRANSPORT_DTLS ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_TRANSPORT_DTLS ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_TRANSPORT_DTLS ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_TRANSPORT_DTLS ADD  updated_at DATETIME DEFAULT NULL;

DROP TABLE IF EXISTS TBL_DELIVERY_DTLS;

CREATE TABLE TBL_DELIVERY_DTLS(
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	INDUSTRY_CODE VARCHAR(5),-- CD_INDUSTRY_CODE
	STOCK_CODE VARCHAR(40),-- CD_STOCK_CODE
	STOCK_VARIETIES_CODE VARCHAR(40),-- CD_STOCK_VARIETY
	DELIVERY_TYPE_CODE VARCHAR(5),-- CD_DELIVERY_TYPE
	DELIVERY_DEPO VARCHAR(5),-- CD_DELIVERY_DEPO
	DELIVERY_LAT_NO BIGINT,
	DELIVERY_ADDRESS VARCHAR(100),
	MORE_DETAILS TEXT,
    PRIMARY KEY (ID)
);

ALTER TABLE TBL_DELIVERY_DTLS ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_DELIVERY_DTLS ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_DELIVERY_DTLS ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_DELIVERY_DTLS ADD  updated_at DATETIME DEFAULT NULL;

DROP TABLE IF EXISTS TBL_CONTRACT;

CREATE TABLE TBL_CONTRACT(
    ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	INDUSTRY_CODE VARCHAR(5),-- CD_INDUSTRY_CODE
	START_DATE DATETIME,
	END_DATE DATETIME,
	CONTRACT_TYPE VARCHAR(5), -- CD_CONTRACT_TYPE
	CONTRACT_NAME VARCHAR(60),
	DELIVERY_PRODUCT VARCHAR(5),-- CD_STOCK_VARIETY
	RAW_MATERIAL VARCHAR(5), -- CD_RAW_MATERIAL
	ALOTED_WMT_ENTRY DECIMAL(10,2),
	ALOTED_WMT_ADJUSTMENT DECIMAL(10,2),
	FINAL_PRODUCT_WMT DECIMAL(10,2),
	LAST_LAT_NO BIGINT,
	PNDNG_PRODUCT_WMT DECIMAL(10,2),
	RECV_RAW_MATRL_WMT DECIMAL(10,2),
	STATUS VARCHAR(5), -- CD_CONTRACT_STATUS
	MORE_DETAILS TEXT,
	PRIMARY KEY (ID)
);

ALTER TABLE TBL_CONTRACT ADD  created_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_CONTRACT ADD  created_at DATETIME NOT NULL;
ALTER TABLE TBL_CONTRACT ADD  updated_by VARCHAR(50) DEFAULT NULL;
ALTER TABLE TBL_CONTRACT ADD  updated_at DATETIME DEFAULT NULL;







