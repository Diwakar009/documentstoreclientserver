/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.model.sms;

import com.docstore.framework.IEntity;
import com.docstore.model.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TBL_CONTRACT database table.
 * 
 */
@Entity
@Table(name = "TBL_CONTRACT")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class Contract extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name = "START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Column(name = "END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@Column(name="CONTRACT_TYPE_CODE", length = 5)
	private String contractTypeCode;

	@Column(name="CONTRACT_NAME", length = 60)
	private String contractName;

	@Column(name="DELIVERY_PRODUCT", length = 5)
	private String DeliveryProduct;

	@Column(name="RAW_MATERIAL", length = 5)
	private String rawMaterial;

	@Column(name="ALOTED_WMT_ENTRY", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double alotedWmtEntry;

	@Column(name="ALOTED_WMT_ADJUSTMENT", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double alotedWmtAdjustment;

	@Column(name="FINAL_PRODUCT_WMT", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double finalProductWmt;

	@Column(name="LAST_LAT_NO", nullable = true, columnDefinition = "BIGINT UNSIGNED")
	private Long lastLatNo;

	@Column(name="PNDNG_PRODUCT_WMT", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double pendingProductWmt;

	@Column(name="RECV_RAW_MATRL_WMT", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double recvRawMatrlWmt;

	@Column(name="STATUS", length = 5)
	private String status;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getContractTypeCode() {
		return contractTypeCode;
	}

	public void setContractTypeCode(String contractTypeCode) {
		this.contractTypeCode = contractTypeCode;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getDeliveryProduct() {
		return DeliveryProduct;
	}

	public void setDeliveryProduct(String deliveryProduct) {
		DeliveryProduct = deliveryProduct;
	}

	public String getRawMaterial() {
		return rawMaterial;
	}

	public void setRawMaterial(String rawMaterial) {
		this.rawMaterial = rawMaterial;
	}

	public Double getAlotedWmtEntry() {
		return alotedWmtEntry;
	}

	public void setAlotedWmtEntry(Double alotedWmtEntry) {
		this.alotedWmtEntry = alotedWmtEntry;
	}

	public Double getAlotedWmtAdjustment() {
		return alotedWmtAdjustment;
	}

	public void setAlotedWmtAdjustment(Double alotedWmtAdjustment) {
		this.alotedWmtAdjustment = alotedWmtAdjustment;
	}

	public Double getFinalProductWmt() {
		return finalProductWmt;
	}

	public void setFinalProductWmt(Double finalProductWmt) {
		this.finalProductWmt = finalProductWmt;
	}

	public Long getLastLatNo() {
		return lastLatNo;
	}

	public void setLastLatNo(Long lastLatNo) {
		this.lastLatNo = lastLatNo;
	}

	public Double getPendingProductWmt() {
		return pendingProductWmt;
	}

	public void setPendingProductWmt(Double pendingProductWmt) {
		this.pendingProductWmt = pendingProductWmt;
	}

	public Double getRecvRawMatrlWmt() {
		return recvRawMatrlWmt;
	}

	public void setRecvRawMatrlWmt(Double recvRawMatrlWmt) {
		this.recvRawMatrlWmt = recvRawMatrlWmt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity entity) {
		Contract source = (Contract)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setId(source.getId());
		this.setIndustryCode(source.getIndustryCode());
		this.setStartDate(source.getStartDate());
		this.setEndDate(source.getEndDate());
		this.setContractTypeCode(source.getContractTypeCode());
		this.setContractName(source.getContractName());
		this.setDeliveryProduct(source.getDeliveryProduct());
		this.setRawMaterial(source.getRawMaterial());
		this.setAlotedWmtEntry(source.getAlotedWmtEntry());
		this.setAlotedWmtAdjustment(source.getAlotedWmtAdjustment());
		this.setFinalProductWmt(source.getFinalProductWmt());
		this.setLastLatNo(source.getLastLatNo());
		this.setPendingProductWmt(source.getPendingProductWmt());
		this.setRecvRawMatrlWmt(source.getRecvRawMatrlWmt());
		this.setStatus(source.getStatus());
		this.setMoreDetails(source.getMoreDetails());
	}
}