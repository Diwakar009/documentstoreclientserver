/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.model.sms;

import com.docstore.framework.IEntity;
import com.docstore.model.BaseEntity;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the TBL_STOCK_DTLS database table.
 *
 */
@Entity
@Table(name = "TBL_STOCK_DTLS")
@AttributeOverride(name = "id", column = @Column(name = "ID", nullable = false,
		columnDefinition = "BIGINT UNSIGNED"))
public class StockDetails extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;


	public StockDetails() {
	}

	public StockDetails(Long id, String industryCode, String stockCode, String stockVarietyCode, String transactionType, Date transactionDate, Long noOfPackets, Double totalWeightment, Character plusOrMinusOnTotal, String remarks, String onBeHalf,String rstNumber,String vehicleNumber) {
		this.id = id;
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.noOfPackets = noOfPackets;
		this.totalWeightment = totalWeightment;
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
		this.remarks = remarks;
		this.onBeHalf = onBeHalf;

		this.transportDetails = new TransportDetails();
		this.transportDetails.setRstNumber(rstNumber);
		this.transportDetails.setVehicleNumber(vehicleNumber);
	}

	public StockDetails(Long id, String industryCode, String stockCode, String stockVarietyCode, String transactionType, Date transactionDate, Long noOfPackets, Double totalWeightment, Character plusOrMinusOnTotal, String remarks, String onBeHalf) {
		this.id = id;
		this.industryCode = industryCode;
		this.stockCode = stockCode;
		this.stockVarietyCode = stockVarietyCode;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.noOfPackets = noOfPackets;
		this.totalWeightment = totalWeightment;
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
		this.remarks = remarks;
		this.onBeHalf = onBeHalf;

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Column(name="INDUSTRY_CODE", length = 5)
	private String industryCode;

	@Column(name="STOCK_CODE", length = 5)
	private String stockCode;

	@Column(name="STOCK_VARIETIES_CODE", length = 5)
	private String stockVarietyCode;

	@Column(name="TRANSACTION_TYPE", length = 5)
	private String transactionType;

	@Column(name="TRANSACTION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	@Column(name = "NO_OF_PACKETS", nullable = true, columnDefinition = "BIGINT SIGNED")
	private Long noOfPackets;

	@Column(name = "TOTAL_WEIGHTMENT", nullable = true, columnDefinition = "DECIMAL SIGNED")
	private Double totalWeightment;

	@Column(name = "PLUS_OR_MINUS_ON_TOTAL", length = 1)
	private Character plusOrMinusOnTotal;

	/*@Column(name = "PKT_OPENING_BAL", nullable = true, columnDefinition = "BIGINT UNSIGNED")
	private Long packetOpeningBalance;

	@Column(name = "WMT_OPENING_BAL", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double weighmentOpeningBalance;

	@Column(name = "PKT_CLOSING_BAL", nullable = true, columnDefinition = "BIGINT UNSIGNED")
	private Long packetClosingBalance;

	@Column(name = "WMT_CLOSING_BAL", nullable = true, columnDefinition = "DECIMAL UNSIGNED")
	private Double weightmentClosingBalance;*/

	@Column(name="REMARKS", length = 100)
	private String remarks;

	@Column(name="ONBEHALF", length = 5)
	private String onBeHalf;

	@Column(name="MORE_DETAILS", length = 65535)
	private String moreDetails;


	@OneToOne
	@JoinColumn(name="PARTY_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private PartyDetails partyDetails;

	@OneToOne
	@JoinColumn(name="TRANSPORT_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private TransportDetails transportDetails;

	@OneToOne
	@JoinColumn(name="DELIVERY_ID")
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private DeliveryDetails deliveryDetails;

	@OneToOne
	@JoinColumns(
			{
					@JoinColumn(updatable=false,insertable=false, name="INDUSTRY_CODE", referencedColumnName="INDUSTRY_CODE"),
					@JoinColumn(updatable=false,insertable=false, name="STOCK_CODE", referencedColumnName="STOCK_CODE"),
					@JoinColumn(updatable=false,insertable=false, name="STOCK_VARIETIES_CODE", referencedColumnName="STOCK_VARIETIES_CODE")
			}
	)
	@Cascade(value = org.hibernate.annotations.CascadeType.REFRESH)
	private Stock stock;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockVarietyCode() {
		return stockVarietyCode;
	}

	public void setStockVarietyCode(String stockVarietyCode) {
		this.stockVarietyCode = stockVarietyCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getNoOfPackets() {
		return noOfPackets;
	}

	public void setNoOfPackets(Long noOfPackets) {
		this.noOfPackets = noOfPackets;
	}

	public Double getTotalWeightment() {
		return totalWeightment;
	}

	public void setTotalWeightment(Double totalWeightment) {
		this.totalWeightment = totalWeightment;
	}

	public Character getPlusOrMinusOnTotal() {
		return plusOrMinusOnTotal;
	}

	public void setPlusOrMinusOnTotal(Character plusOrMinusOnTotal) {
		this.plusOrMinusOnTotal = plusOrMinusOnTotal;
	}

	/*public Long getPacketOpeningBalance() {
		return packetOpeningBalance;
	}

	public void setPacketOpeningBalance(Long packetOpeningBalance) {
		this.packetOpeningBalance = packetOpeningBalance;
	}

	public Double getWeighmentOpeningBalance() {
		return weighmentOpeningBalance;
	}

	public void setWeighmentOpeningBalance(Double weighmentOpeningBalance) {
		this.weighmentOpeningBalance = weighmentOpeningBalance;
	}

	public Long getPacketClosingBalance() {
		return packetClosingBalance;
	}

	public void setPacketClosingBalance(Long packetClosingBalance) {
		this.packetClosingBalance = packetClosingBalance;
	}

	public Double getWeightmentClosingBalance() {
		return weightmentClosingBalance;
	}

	public void setWeightmentClosingBalance(Double weightmentClosingBalance) {
		this.weightmentClosingBalance = weightmentClosingBalance;
	}*/

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOnBeHalf() {
		return onBeHalf;
	}

	public void setOnBeHalf(String onBeHalf) {
		this.onBeHalf = onBeHalf;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public PartyDetails getPartyDetails() {
		return partyDetails;
	}

	public void setPartyDetails(PartyDetails partyDetails) {
		this.partyDetails = partyDetails;
	}

	public TransportDetails getTransportDetails() {
		return transportDetails;
	}

	public void setTransportDetails(TransportDetails transportDetails) {
		this.transportDetails = transportDetails;
	}

	public DeliveryDetails getDeliveryDetails() {
		return deliveryDetails;
	}

	public void setDeliveryDetails(DeliveryDetails deliveryDetails) {
		this.deliveryDetails = deliveryDetails;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	@Override
	public Serializable getEntityPK() {
		return getId();
	}

	@Override
	public void copyEntity(IEntity entity) {
		StockDetails source = (StockDetails)entity;
		this.setCreatedBy(source.getCreatedBy());
		this.setUpdatedBy(source.getUpdatedBy());
		this.setCreatedAt(source.getCreatedAt());
		this.setUpdatedAt(source.getUpdatedAt());
		this.setIndustryCode(source.getIndustryCode());
		this.setStockCode(source.getStockCode());
		this.setStockVarietyCode(source.getStockVarietyCode());
		this.setTransactionType(source.getTransactionType());
		this.setTransactionDate(source.getTransactionDate());
		this.setNoOfPackets(source.getNoOfPackets());
		this.setTotalWeightment(source.getTotalWeightment());
		this.setPlusOrMinusOnTotal(source.getPlusOrMinusOnTotal());
		/*this.setPacketOpeningBalance(source.getPacketOpeningBalance());
		this.setWeighmentOpeningBalance(source.getWeighmentOpeningBalance());
		this.setPacketClosingBalance(source.getPacketClosingBalance());
		this.setWeightmentClosingBalance(source.getWeightmentClosingBalance());*/
		this.setRemarks(source.getRemarks());
		this.setOnBeHalf(source.getOnBeHalf());
		this.setRemarks(source.getRemarks());
		this.setMoreDetails(source.getMoreDetails());

		if(this.partyDetails != null){
			this.partyDetails.copyEntity(source.getPartyDetails());
		}

		if(this.deliveryDetails != null){
			this.deliveryDetails.copyEntity(source.getDeliveryDetails());
		}

		if(this.transportDetails != null){
			this.transportDetails.copyEntity(source.getTransportDetails());
		}

	}

	@Override
	public String toString() {
		return "StockDetails{" +
				"industryCode='" + industryCode + '\'' +
				", stockCode='" + stockCode + '\'' +
				", stockVarietyCode='" + stockVarietyCode + '\'' +
				", transactionType='" + transactionType + '\'' +
				", noOfPackets=" + noOfPackets +
				", totalWeightment=" + totalWeightment +
				'}';
	}
}