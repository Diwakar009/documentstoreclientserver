package com.docstore.framework;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public interface IEntity {
	public Serializable getEntityPK();

	public void copyEntity(IEntity entity);
}
