/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended;

import com.docstore.jpa.spring.extended.custom.ExtendedRepository;
import com.docstore.model.sms.PartyDetails;
import com.docstore.model.sms.StockDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface PartyDetailsRepository extends ExtendedRepository<PartyDetails, Long> {

}
