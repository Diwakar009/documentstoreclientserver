package com.docstore.jpa.spring.extended.config;

import com.docstore.jpa.spring.extended.custom.ExtendedRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by diwakar009 on 8/12/2018.
 */
@Configuration
@PropertySource(value= {"classpath:app.properties"})
@EnableJpaRepositories(basePackages = "com.docstore.jpa.spring.extended",
        repositoryBaseClass = ExtendedRepositoryImpl.class)
public class ExtendedRepositoryConfig<T> {

    @Autowired
    Environment environment;

   /* @Bean (name = "dataSource")
    @Primary*/
    public DataSource getDataSource() throws SQLException {
        DriverManagerDataSource source = new DriverManagerDataSource();
        /*
            source.setDriverClassName("com.mysql.jdbc.Driver");
            source.setUsername("root");
            source.setPassword("root");
            source.setUrl("jdbc:mysql://localhost:3306/docstore_desktopdb?autoReconnect=true&useSSL=false");
        */
       /* System.out.println("###########$$$$$$$$$$$$ " + environment.getProperty("spring.datasource.driverclassname") );*/

        source.setDriverClassName(environment.getProperty("spring.datasource.driverclassname"));
        source.setUsername(environment.getProperty("spring.datasource.username"));
        source.setPassword(environment.getProperty("spring.datasource.password"));
        source.setUrl(environment.getProperty("spring.datasource.url"));

        return source;
    }

 /*   @Bean (name = "dataSource")
    @Primary
    public DataSource embedDerbyDataSource() throws SQLException {
        DriverManagerDataSource source =new DriverManagerDataSource();
        source.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
        source.setUsername("root");
        source.setPassword("root");
        source.setUrl("jdbc:derby:./db/documentum_desktopdb;create=true");
        return source;
    }

    @Bean (name = "dataSource")
    @Qualifier
    public DataSource embedServerDataSource() throws SQLException {
        DriverManagerDataSource source =new DriverManagerDataSource();
        source.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
        source.setUsername("root");
        source.setPassword("root");
        source.setUrl("jdbc:derby://localhost:1527/documentum_desktopdb");
        return source;
    }*/

    @Bean
    public EntityManagerFactory entityManagerFactory() throws SQLException {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan(environment.getProperty("spring.entity.model"));
        factory.setDataSource(getDataSource());
        //factory.setJpaProperties(additionalProperties());

        HashMap<String,Object> jpaprop = new HashMap<String,Object>();
        jpaprop.put("hibernate.dialect",environment.getProperty("hibernate.dialect"));
        jpaprop.put("hibernate.show_sql",environment.getProperty("hibernate.show_sql"));
        jpaprop.put("hibernate.format_sql",environment.getProperty("hibernate.format_sql"));
        jpaprop.put("hibernate.hbm2ddl.auto",environment.getProperty("hibernate.ddl-auto"));
        factory.setJpaPropertyMap(jpaprop);

        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

  /*  private Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
        properties.setProperty("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.setProperty("hibernate.current_session_context_class", env.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));
        properties.setProperty("hibernate.jdbc.lob.non_contextual_creation", env.getProperty("spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation"));
        properties.setProperty("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
        properties.setProperty("hibernate.format_sql", env.getProperty("spring.jpa.properties.hibernate.format_sql"));
        return properties;
    }*/

}
