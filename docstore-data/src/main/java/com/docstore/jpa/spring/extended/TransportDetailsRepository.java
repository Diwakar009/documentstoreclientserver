/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended;

import com.docstore.jpa.spring.extended.custom.ExtendedRepository;
import com.docstore.model.sms.StockDetails;
import com.docstore.model.sms.TransportDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface TransportDetailsRepository extends ExtendedRepository<TransportDetails, Long> {

}
