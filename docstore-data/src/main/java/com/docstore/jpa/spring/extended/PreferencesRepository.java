package com.docstore.jpa.spring.extended;

import com.docstore.jpa.spring.extended.custom.ExtendedRepository;
import com.docstore.model.Preferences;
import org.springframework.stereotype.Repository;

/**
 * Created by diwakar009 on 18/12/2018.
 */
@Repository
public interface PreferencesRepository extends ExtendedRepository<Preferences,Long> {

}
