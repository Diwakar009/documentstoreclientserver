/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended;

import com.docstore.jpa.spring.extended.custom.ExtendedRepository;
import com.docstore.model.StaticCodeDecode;
import com.docstore.model.StaticCodeDecodePK;
import com.docstore.model.sms.Industry;
import com.docstore.model.sms.IndustryPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndustryRepository extends ExtendedRepository<Industry, IndustryPK> {

}
