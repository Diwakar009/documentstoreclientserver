/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended.extention.impl;

import com.docstore.jpa.spring.extended.custom.CustomJpaRepository;
import com.docstore.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.docstore.model.sms.*;
import org.hibernate.query.criteria.internal.path.SingularAttributeJoin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import java.util.*;

/**
 * Created by diwakar009 on 22/10/2019.
 */
@Repository
public class StockDetailsExtendedRepositoryImpl implements StockDetailsExtendedRepository{

    private final Logger logger = LoggerFactory.getLogger(StockDetailsExtendedRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<StockDetails> findAllStockDetailsByFilter(String industryCodeFilter, String stockCodeFilter, String stockVarietyCodeFilter,
                                                          String transTypeFilter,String rstNumberFilter,String vehicleNumberFilter, Date startDateFilter, Date endDateFilter, Pageable pageRequest) {
        logger.info("Executing findallstockdetailsByFilter...");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<StockDetails> mainCreteriaQuery = criteriaBuilder.createQuery(StockDetails.class);
        CriteriaQuery<Long> countCriteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<StockDetails> mainRoot = mainCreteriaQuery.from(StockDetails.class);
        Root<StockDetails> countRoot = countCriteriaQuery.from(StockDetails.class);

        Join<StockDetails,TransportDetails> transportDetailsJoin = null;
        Join<StockDetails,PartyDetails> partyDetailsJoin = null;

        transportDetailsJoin = mainRoot.join(StockDetails_.transportDetails, JoinType.LEFT); // Join with transport details to main query
        partyDetailsJoin = mainRoot.join(StockDetails_.partyDetails, JoinType.LEFT); // Join with party details to main query

        countRoot.join(StockDetails_.transportDetails, JoinType.LEFT); // Join with transport details to count query
        countRoot.join(StockDetails_.partyDetails, JoinType.LEFT); // Join with transport details to count query

        mainCreteriaQuery.select(criteriaBuilder.construct(StockDetails.class,
                    mainRoot.get("id"), mainRoot.get("industryCode"), mainRoot.get("stockCode"), mainRoot.get("stockVarietyCode"),
                    mainRoot.get("transactionType"), mainRoot.get("transactionDate"), mainRoot.get("noOfPackets"), mainRoot.get("totalWeightment"),
                    mainRoot.get("plusOrMinusOnTotal"), mainRoot.get("remarks"), mainRoot.get("onBeHalf"),(transportDetailsJoin.get("rstNumber")).alias("rstNumber"),
                    (transportDetailsJoin.get("vehicleNumber")).alias("vehicleNumber")));


        Optional<ParameterExpression<String>> icFilter = Optional.empty();
        Optional<ParameterExpression<String>> scFilter = Optional.empty();
        Optional<ParameterExpression<String>> svcFilter = Optional.empty();
        Optional<ParameterExpression<String>> tyFilter = Optional.empty();
        Optional<ParameterExpression<String>> rstNoFilter = Optional.empty();
        Optional<ParameterExpression<String>> vehicleNoFilter = Optional.empty();
        Optional<ParameterExpression<Date>> sdFilter = Optional.empty();
        Optional<ParameterExpression<Date>> edFilter = Optional.empty();

        List<Predicate> predicates = new ArrayList<Predicate>();

        if(industryCodeFilter != null && !"".equals(industryCodeFilter)) {
            icFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("industryCode"), icFilter.get()));
        }

        if(stockCodeFilter != null && !"".equals(stockCodeFilter)) {
            scFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("stockCode"),scFilter.get()));
        }

        if(stockVarietyCodeFilter != null && !"".equals(stockVarietyCodeFilter)) {
            svcFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("stockVarietyCode"),svcFilter.get()));
        }

        if(transTypeFilter != null && !"".equals(transTypeFilter)) {
            tyFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.equal(mainRoot.get("transactionType"),tyFilter.get()));
        }

        if(rstNumberFilter != null && !"".equals(rstNumberFilter)) {
            rstNoFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(transportDetailsJoin.get(TransportDetails_.rstNumber),rstNoFilter.get()));
        }

        if(vehicleNumberFilter != null && !"".equals(vehicleNumberFilter)) {
            vehicleNoFilter = Optional.ofNullable(criteriaBuilder.parameter(String.class));
            predicates.add(criteriaBuilder.like(transportDetailsJoin.get(TransportDetails_.vehicleNumber),vehicleNoFilter.get()));
        }

        if(startDateFilter != null && endDateFilter != null){
            sdFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            edFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
            predicates.add(criteriaBuilder.between(mainRoot.get("transactionDate"),sdFilter.get(),edFilter.get()));

        }else {
            if (startDateFilter != null) {
                sdFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.equal(mainRoot.get("transactionDate"), sdFilter.get()));
            }

            if (endDateFilter != null) {
                edFilter = Optional.ofNullable(criteriaBuilder.parameter(Date.class));
                predicates.add(criteriaBuilder.equal(mainRoot.get("transactionDate"), edFilter.get()));
            }
        }

        mainCreteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        TypedQuery<StockDetails> mainQuery = entityManager.createQuery(mainCreteriaQuery);

        // Count Query
        countCriteriaQuery.select(criteriaBuilder.count(countRoot));
        countCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()])); // Apply where clause
        TypedQuery<Long> countQuery = entityManager.createQuery(countCriteriaQuery);

       if(icFilter.isPresent()) {
           mainQuery.setParameter(icFilter.get(), industryCodeFilter);
           countQuery.setParameter(icFilter.get(), industryCodeFilter);
       }

       if(scFilter.isPresent()) {
           mainQuery.setParameter(scFilter.get(), stockCodeFilter);
           countQuery.setParameter(scFilter.get(), stockCodeFilter);
       }

       if(svcFilter.isPresent()) {
            mainQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
            countQuery.setParameter(svcFilter.get(), stockVarietyCodeFilter);
       }

        if(tyFilter.isPresent()) {
            mainQuery.setParameter(tyFilter.get(), transTypeFilter);
            countQuery.setParameter(tyFilter.get(), transTypeFilter);
        }

        if(rstNoFilter.isPresent()) {
            mainQuery.setParameter(rstNoFilter.get(), "%"+ rstNumberFilter + "%");
            countQuery.setParameter(rstNoFilter.get(), "%"+ rstNumberFilter + "%");
        }

        if(vehicleNoFilter.isPresent()) {
            mainQuery.setParameter(vehicleNoFilter.get(), "%"+ vehicleNumberFilter + "%");
            countQuery.setParameter(vehicleNoFilter.get(), "%"+ vehicleNumberFilter + "%");
        }

        if(sdFilter.isPresent()) {
            mainQuery.setParameter(sdFilter.get(), startDateFilter);
            countQuery.setParameter(sdFilter.get(), startDateFilter);
        }

        if(edFilter.isPresent()) {
            mainQuery.setParameter(edFilter.get(), endDateFilter);
            countQuery.setParameter(edFilter.get(), endDateFilter);
        }

        long total = countQuery.getSingleResult();
        return (Page)(pageRequest == null?new PageImpl(mainQuery.getResultList()):this.readPage(mainQuery,pageRequest, total));
    }

    protected Page<StockDetails> readPage(TypedQuery<StockDetails> query, Pageable pageable, Long total) {
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        List<StockDetails> content = total.longValue() > (long)pageable.getOffset()?query.getResultList(): Collections.emptyList();
        return new PageImpl(content, pageable, total.longValue());
    }

}
