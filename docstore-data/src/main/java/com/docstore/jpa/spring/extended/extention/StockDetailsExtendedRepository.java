/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended.extention;

import com.docstore.model.sms.StockDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * Created by diwakar009 on 22/10/2019.
 */
public interface StockDetailsExtendedRepository {
    public Page<StockDetails> findAllStockDetailsByFilter(String industryCodeFilter, String stockCodeFilter,
                                                          String stockVarietyCodeFilter,String transTypeFilter,
                                                          String rstNumberFilter,String vehicleNumberFilter,
                                                          Date startDateFilter, Date endDateFilter, Pageable pageRequest);
}
