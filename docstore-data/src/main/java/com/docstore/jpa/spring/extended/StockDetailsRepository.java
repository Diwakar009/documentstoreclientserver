/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended;

import com.docstore.jpa.spring.extended.custom.ExtendedRepository;
import com.docstore.jpa.spring.extended.extention.StockDetailsExtendedRepository;
import com.docstore.model.StaticCodeDecode;
import com.docstore.model.sms.Stock;
import com.docstore.model.sms.StockDetails;
import com.docstore.model.sms.StockPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface StockDetailsRepository extends ExtendedRepository<StockDetails, Long> ,StockDetailsExtendedRepository {

    /**
     * Finds the stock details with limited fields.
     *
     * @param pagable
     * @return
     */

    @Query("SELECT new StockDetails(s.id, s.industryCode, s.stockCode, s.stockVarietyCode, s.transactionType, s.transactionDate, s.noOfPackets, s.totalWeightment, " +
            " s.plusOrMinusOnTotal, s.remarks, s.onBeHalf) FROM StockDetails s " +
            " order by  s.transactionDate desc")
    public Page<StockDetails> findAllStockDetails(Pageable pagable);



}
