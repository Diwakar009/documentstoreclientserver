/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.jpa.spring.extended;

import com.docstore.jpa.spring.extended.custom.ExtendedRepository;
import com.docstore.model.sms.Contract;
import com.docstore.model.sms.Stock;
import com.docstore.model.sms.StockPK;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractRepository extends ExtendedRepository<Contract, Long> {

}
