/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.data;

import com.docstore.jpa.spring.extended.StockDetailsRepository;
import com.docstore.jpa.spring.extended.StockRepository;
import com.docstore.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.docstore.model.StaticCodeDecode;
import com.docstore.model.sms.StockDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class StockDetailsRepositoryTest {

    @Resource
    private StockDetailsRepository stockDetailsRepository;

    @Before
    public void setup() {


    }

    @Test
    public void dataSetup(){



    }

    @Test
    public void testfindAllFilter(){

        String industryCodeFilter = "";
        String stockCodeFilter = "";
        String stockVarietyCodeFilter = "";
        String transTypeFilter = "";
        String rstNumberFilter ="";
        String vehicleNumberFilter="";
        Date transactionDate = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        try {
            transactionDate = df.parse("20/10/2019");
        }catch (ParseException exception)
        {
            System.out.println("Parse exception....");
        }

        transactionDate = null;

        Page<StockDetails> stockDetails = stockDetailsRepository.findAllStockDetailsByFilter(industryCodeFilter,stockCodeFilter,stockVarietyCodeFilter,transTypeFilter,
                rstNumberFilter,vehicleNumberFilter,transactionDate,null, PageRequest.of(0, 10));

        System.out.println("*********************************" + stockDetails.getTotalElements());

    }

    @Test
    public void testfindAllStartNEndDateFilter(){

       /* String industryCodeFilter = "KMR";
        String stockCodeFilter = "PADD";
        String stockVarietyCodeFilter = "ODPY";
        String transTypeFilter = "RECV";
        String rstNumber ="1234";*/
        String industryCodeFilter = "";
        String stockCodeFilter = "";
        String stockVarietyCodeFilter = "";
        String transTypeFilter = "";
        String rstNumber ="1234";
        String vehicleNumber = "";
        Date startDate = new Date();
        Date endDate = new Date();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        try {
            startDate = df.parse("19/10/2019");
            endDate = df.parse("20/10/2019");
        }catch (ParseException exception){
            System.out.println("Parse exception....");
        }

        startDate= null;
        endDate=null;

        Page<StockDetails> stockDetails = stockDetailsRepository.findAllStockDetailsByFilter(industryCodeFilter,stockCodeFilter,stockVarietyCodeFilter,transTypeFilter,rstNumber,"",startDate,endDate, PageRequest.of(0, 10));

        System.out.println("*********************************" + stockDetails.getTotalElements());

    }





}
