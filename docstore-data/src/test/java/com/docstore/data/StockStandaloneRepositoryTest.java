/*
 * Copyright (c) 2019.
 *
 * All Rights are with Diwakar C
 */

package com.docstore.data;

import com.docstore.jpa.spring.extended.IndustryRepository;
import com.docstore.jpa.spring.extended.StockRepository;
import com.docstore.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.docstore.model.sms.Industry;
import com.docstore.model.sms.Stock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class StockStandaloneRepositoryTest {

    @Resource
    private StockRepository stockRepository;

    @Before
    public void setup() {


    }

    @Test
    public void dataSetup(){


    }





}
