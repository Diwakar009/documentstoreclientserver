package com.docstore.data;

import com.docstore.jpa.spring.extended.PreferencesRepository;
import com.docstore.jpa.spring.extended.config.ExtendedRepositoryConfig;
import com.docstore.model.Preferences;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by diwakar009 on 17/12/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ExtendedRepositoryConfig.class })
public class PreferenceStandaloneRepositoryTest {

    @Resource
    private PreferencesRepository preferencesRepository;

    @Before
    public void setup() {

    }
    @Test
    public void getDefaultPreference(){
        Preferences preferences
                = (preferencesRepository.findById(1l)).get();

        System.out.println("Tip of the day " +preferences.isShowTipOfTheDay());
    }

    @Test
    //@Transactional
    public void updateDefaultPreference(){
        Preferences preferences
                = (preferencesRepository.findById(1l)).get();
        preferences.setShowTipOfTheDay(false);

        preferencesRepository.save(preferences);
        Preferences newPreferences
                = (preferencesRepository.findById(1l)).get();

        System.out.println("Tip of the day " +newPreferences.isShowTipOfTheDay());
    }

}
